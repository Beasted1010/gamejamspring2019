
#ifndef COMMON_H
#define COMMON_H

#include "stdint.h"

#ifdef _WIN32
#define __USE_MINGW_ANSI_STDIO 1
#endif

#define FRAMERATE 60

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

#define MOUSE_LDOWN (1 << 0)
#define MOUSE_RDOWN (1 << 1)
#define MOUSE_LUP (1 << 2)
#define MOUSE_RUP (1 << 3)

#define SIZE_ARRAY( arr ) (( sizeof(arr) / sizeof(arr[0]) ))

extern char error_buf[256];

typedef struct RGB
{
    uint8_t r, g, b;
} RGB;

#endif
