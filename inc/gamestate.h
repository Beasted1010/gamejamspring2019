
#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "vulkan_wrapper.h"
#include "matrix.h"
#include "stdint.h"

#define MAP_MAX_X 200
#define MAP_MAX_Y 200

#define MAX_DIFFICULTY 5

#define MAX_ENEMY_COUNT 64
#define MAX_FIREBALL_COUNT 64
#define MAX_PICKUP_COUNT 10

#define FIREBALL_START_RADIUS 1
#define FIREBALL_START_MASS 0.2

#define FIREBALL_EXISTANCE_SECONDS 10
#define FIREBALL_SPEED_MULTIPLIER 10

#define ENEMY_START_MASS 1
#define ENEMY_START_RADIUS 1

#define ENEMY_SPEED 5
#define ENEMY_ACCEL 1
#define START_ENEMY_SPAWN_RATE 1

#define STRENGTH_INCREASE 35
#define MAX_STRENGTH 140

#define MAX_VELOCITY 40
#define CONSTANT_ACCELERATION 2
#define INITIAL_ACCELERATION 1
#define INITIAL_SPEED 1

#define FRICTION 0.3f

#define PLAYER_TURN_SPEED 0.003

#define PLAYER_START_DIRECTION 0

#define PLAYER_START_X 0
#define PLAYER_START_Y 0

#define MASS_IMPACT_TO_SPEED_FACTOR 1

#define PLAYER_START_MASS 3
#define PLAYER_START_RADIUS 3

#define PLAYER_HIT_MASS_LOSS 0.5
#define PLAYER_HIT_RADIUS_LOSS 0.5

#define FIREBALL_SHOT_DELAY_SECONDS 0.4

// REGION: Score
#define TIME_ELAPSE_SCORE_BOOST_MULTIPLIER 30
#define PLAYER_HIT_PENALTY 50
#define ICE_CUBE_PICKUP_SCORE_BOOST 100
#define LAUNCH_FIREBALL_PENALTY_MULTIPLIER 1
#define ENEMY_KILL_BOOST 100

#define DIFFICULTY_INCREASE 0.5

#define SECONDS_ELAPSE_FOR_PICKUP_SPAWN 3
#define SECONDS_ELAPSE_FOR_DIFFICULTY_SCORE_BOOST 1
#define SECONDS_ELAPSE_FOR_DIFFICULTY_JUMP 15

#define MAX_PLAYER_MASS 10

// REGION: ENEMY PERSONALITIES
// However large player is, make perimeter this much larger
#define PLAYER_PERIMETER_RADIUS_MULTIPLIER 2

#define RETREAT_TIMEOUT_SECONDS 5
#define TEASE_TIMEOUT_SECONDS 4

#define COURSE_TIMEOUT_SECONDS 6
#define COURSE_RECOVERY_TIMEOUT_SECONDS 2



// REGION: PICKUPS
#define INITIAL_PICKUP_COUNT 4

#define ICE_CUBE_PICKUP_RADIUS 1
#define ICE_CUBE_PICKUP_MASS_BOOST 1




#define ENEMY_MOVE_DELAY 0

#define MAX_ACCEL 3


enum PlayerKeys
{
    KeyFireBackwards,
    KeyNum
};

enum EnemyPersonalities
{
    NO_ENEMY_PERSONALITY,
    HEADHUNTER,
    TEASER,
    NARROWSIGHT,
    ROBBER,
    TERRORIST,

    PERSONALITY_COUNT
};

enum EnemyModes
{
    IDLE_MODE,
    RETREAT_MODE,
    TEASE_MODE,
    EXECUTE_COURSE_MODE,
    PLAN_COURSE_MODE,
    THIEF_MODE,
    SUICIDE_MODE,

    ENEMY_MODE_COUNT
};

enum PickupType
{
    NO_PICKUP,
    ICE_CUBE_PICKUP,

    PICKUP_TYPE_COUNT
};

struct FloatRect
{
    Vector2 min, max;
};

struct FloatCircle
{
    Vector2 position;
    float radius;
};

struct Input
{
    int pause;
    int quit;
    int16_t mousex, mousey;
    uint8_t keydown[KeyNum];
    uint8_t mousestate;
};

struct Entity
{
    Vector2 position;
    Vector2 velocity;
    Vector2 accel;

    float radius;
    float mass;
};

struct Fireball
{
    struct Entity entity;
    Matrix4 model;

    float seconds_existed;
};

struct Player
{
    struct Entity entity;
    struct FloatCircle perimeter_circle;
    Matrix4 model;

    struct Fireball loadedFireball;

    uint16_t score;
    uint32_t kill_count;

    float last_fireball_shot_time_elapse;

    float direction; // Radians from x = 1, y = 0
    float strength; // 0 => no strength, increases with mouse hold
    uint8_t fire_backward;
};

struct MapData
{
    struct FloatRect rect;
};

struct RenderingInfo
{
    // This is vulkan data that may be
    // generated/swapped out at runtime
    // and is used for Vulkan rendering
    // operations.

    // Maps a position in world space to a position
    // in the camera's coordinate system.
    // Need one of these per camera.
    Matrix4 view;

    // Maps a position in camera space to a location
    // on the screen. Need one per screen.
    Matrix4 projection;

    // Clips the output of a projection to the dimensions of the screen.
    // Need one per screen.
    Matrix4 clip;

    // The sampler used to read colors from the image itself
    VkSampler texture_sampler;

    // The uniform buffer that will hold the model.
    UniformBuffer model_buffer;

    // The uniform buffer that will hold the product of the above 3 matrices.
    UniformBuffer view_buffer;

    // The image storing the texture of the ice cube.
    VulkanImage ice_texture;
    VulkanImage map_texture;
    VulkanImage enemy_texture;
    VulkanImage sling_texture;
    VulkanImage fireball_texture;
    
    // The vertex buffer for holding the faces of the cube, decomposed
    // into triangles.
    VertexBuffer cube_vertices;
    VertexBuffer square_vertices;
    VertexBuffer sling_vertices;
    VertexBuffer fireball_vertices;

    VulkanFont font;
};

struct Enemy
{
    struct Entity entity;
    Matrix4 model;

    enum EnemyPersonalities personality;
    enum EnemyModes mode;

    float movement_delay_time_elapsed;

    float time_elapsed;
};

struct Pickup
{
    enum PickupType type;

    struct FloatCircle circle_body;
};

struct GameState
{
    struct RenderingInfo rendering;
    struct Enemy enemies[MAX_ENEMY_COUNT];
    struct Fireball fireballs[MAX_FIREBALL_COUNT];
    struct Pickup pickups[MAX_PICKUP_COUNT];
    struct Player player;
    struct MapData map_data;
    struct Input input;
    uint64_t logical_frames;
    uint32_t fps;
    uint32_t current_enemy_count;
    uint16_t current_fireball_count;
    uint16_t current_pickup_count;
    float pickup_spawn_time_elapsed;
    float spawn_time_elapsed;
    float difficulty_jump_time_elapsed; // TODO: Cap?
    float last_enemy_spawn_time;
    float difficulty; // Enemies/second, starts below 1
    uint8_t playing;
};


void CreateGameState( struct GameState* state );
void UpdateGameState( struct GameState* state, float deltaTime );
void RenderGameState( struct GameState* state, float delta_time );
void DestroyGameState( struct GameState* state );




#endif
