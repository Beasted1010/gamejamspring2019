
#ifndef LIST_MALLOC_H
#define LIST_MALLOC_H

// The following code makes some kind of argument about the C preprocessor,
// but I'm not sure which side it is on.

#define _RESULT1( name ) name =
#define _RESULT0( name ) name = 0;
#define _HASRESULT( name, bit ) _RESULT##bit(name)

#define DeclareVulkanListMallocFunction( type, fn, argtype, id, str, haserr, ... )  \
type* Get##id##Malloc( argtype a, uint32_t* count )

#define VulkanListMallocFunction( type, fn, argtype, id, str, haserr, ... )         \
DeclareVulkanListMallocFunction( type, fn, argtype, id, str, haserr, __VA_ARGS__)   \
{                                                                                   \
    uint32_t cur_count;                                                             \
    type* list = NULL;                                                              \
    VkResult _HASRESULT( res, haserr ) fn( __VA_ARGS__, &cur_count, NULL );         \
                                                                                    \
    if ( !cur_count )                                                               \
    {                                                                               \
        init_log( "Error: Found no usable " str ".\n" );                            \
        return NULL;                                                                \
    }                                                                               \
    else if ( res )                                                                 \
    {                                                                               \
        init_log( "Error getting " str " count.\n" );                               \
    }                                                                               \
    else                                                                            \
    {                                                                               \
        init_log( "Found %u object(s) of type " str ".\n", cur_count );             \
    }                                                                               \
                                                                                    \
    list = malloc( sizeof(*list) * cur_count );                                     \
    if ( !list )                                                                    \
    {                                                                               \
        init_log( "Failed to allocate memory for " str " list.\n" );                \
        return NULL;                                                                \
    }                                                                               \
                                                                                    \
    _HASRESULT( res, haserr ) fn( __VA_ARGS__, &cur_count, list );                  \
    if ( res )                                                                      \
    {                                                                               \
        init_log( "Error getting " str " list.\n" );                                \
        free( list );                                                               \
        return NULL;                                                                \
    }                                                                               \
    else                                                                            \
    {                                                                               \
        init_log( "Allocated " str " list.\n" );                                    \
    }                                                                               \
                                                                                    \
    if ( count )                                                                    \
    {                                                                               \
        *count = cur_count;                                                         \
    }                                                                               \
                                                                                    \
    return list;                                                                    \
}

// This replaces the following constructs:
#if 0
VkPhysicalDevice* GetPhysicalDevicesMalloc( VkInstance inst, uint32_t* count )
{
    uint32_t gpu_count;
    VkPhysicalDevice* gpus = NULL;
    VkResult res = vkEnumeratePhysicalDevices( inst, &gpu_count, NULL );

    if ( !gpu_count )
    {
        printf( "Error: Found no usable physical devices.\n" );
        return NULL;
    }
    else if ( res )
    {
        printf( "Error getting GPU count.\n" );
        return NULL;
    }
    else
    {
        printf( "Found %u physical devices.\n", gpu_count );
    }

    gpus = malloc( sizeof( *gpus ) * gpu_count );
    if ( !gpus )
    {
        printf( "Failed to allocate memory for physical device list.\n" );
        return NULL;
    }

    res = vkEnumeratePhysicalDevices( inst, &gpu_count, gpus );
    if ( res )
    {
        printf( "Error getting list of physical devices.\n" );
        free( gpus );
        return NULL;
    }

    if ( count )
    {
        *count = gpu_count;
    }

    return gpus;
}

VkQueueFamilyProperties* GetQueueFamilyPropertiesMalloc( VkPhysicalDevice dev, uint32_t* count )
{
    uint32_t queue_family_count;
    VkQueueFamilyProperties* families = NULL;
    vkGetPhysicalDeviceQueueFamilyProperties( dev, &queue_family_count, NULL );

    if ( !queue_family_count )
    {
        printf( "Error: Found no usable queue families.\n" );
        return NULL;
    }
    else
    {
        printf( "Found %u queue families.\n", queue_family_count );
    }

    families = malloc( sizeof( *families ) * queue_family_count );
    if ( !families )
    {
        printf( "Failed to allocate memory for queue family properties list.\nW" );
        return NULL;
    }

    vkGetPhysicalDeviceQueueFamilyProperties( dev, &queue_family_count, families );

    if ( count )
    {
        *count = queue_family_count;
    }

    return families;
}

VkSurfaceFormatKHR* GetSurfaceFormatsMalloc( VkPhysicalDevice dev, uint32_t* count )
{
    uint32_t surface_format_count;
    VkSurfaceFormatKHR* surface_formats = NULL;
    VkResult res = vkGetPhysicalDeviceSurfaceFormatsKHR( dev, &surface_format_count, NULL );

    if ( !res )
    {
        printf( "Could not get surface format count.\n" );
        return NULL;
    }
    else
    {
        printf( "Found %u supported surface formats.\n" );
    }

    surface_formats = malloc( sizeof( *surface_formats ) * queue_family_count );
    if ( !surface_formats )
    {
        printf( "Failed to allocate memory for surface format list.\n" );
        return NULL;
    }

    res = vkGetPhysicalDeviceSurfacePresentModesKHR( dev, &surface_format_count, surface_formats );
    if ( res )
    {
        printf( "Failed to get list of supported surface formats.\n" );
        free( surface_formats );
        return NULL;
    }

    if ( count )
    {
        *count = surface_format_count;
    }

    return surface_formats;
}
#endif

#endif
