#ifndef PLATFORM_H
#define PLATFORM_H

#include "common.h"
#include "platform_vulkan.h"
#include "gamestate.h"

typedef struct Bitmap Bitmap;

// Window functions
WindowInfo* InitializeWindowInfo( const char* name, uint32_t width, uint32_t height );
void DestroyWindowInfo( WindowInfo* window );

uint32_t GetWidth( const WindowInfo* window );
uint32_t GetHeight( const WindowInfo* window );

void DoWindowRenderUpdate( WindowInfo* window );

// Bitmap functions
Bitmap* LoadBitmapFromFile( const char* filename, const RGB* colorkey );
void FreeBitmap( Bitmap* bmp );
void* GetBitmapPixels( const Bitmap* bmp );
uint32_t GetBitmapWidth( const Bitmap* bmp );
uint32_t GetBitmapHeight( const Bitmap* bmp );

// Event functions
void ProcessEvents( struct Input* input );

// Timing functions
uint64_t GetTicks();
uint64_t GetTickFrequency();

// File I/O
uint32_t* ReadBinaryFileMalloc( const char* filename, size_t* size );
size_t ReadFileSize( const char* filename );

#endif
