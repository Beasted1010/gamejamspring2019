
#ifndef UTIL_H
#define UTIL_H

#include "stdint.h"
#include "stdlib.h"

#include "common.h"
#include "windows.h"

uint64_t GetTickFrequency( );
uint64_t GetTicks( );

size_t CalculateFileSize( HANDLE file );
int ReadBinaryFile( HANDLE file, uint32_t* buf, size_t bufsize, size_t* readsize );
uint32_t* ReadBinaryFileMalloc( const char* filename, size_t* size );

#endif

