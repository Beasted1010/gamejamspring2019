INC = inc
SRC = src
OBJ = obj
SHADERS_SRC = shaders_src
SHADERS_OBJ = shaders

OUT = run.exe

CC = gcc
CFLAGS = -Wall -DLOG_LEVEL=LOG_QUIET -Iinc -I$(VULKAN_SDK)/Include -Ofast
LINKFLAGS = -L$(VULKAN_SDK)/Lib -lvulkan-1

DEPS = $(wildcard $(INC)/*.h)
SOURCES = $(wildcard $(SRC)/*.c)
SHADERS = $(wildcard $(SHADERS_SRC)/*)

OBJECTS = $(subst .c,.o, $(patsubst $(SRC)/%, $(OBJ)/%, $(SOURCES)))
SHADER_OBJECTS = $(patsubst $(SHADERS_SRC)/%, $(SHADERS_OBJ)/%.spv, $(SHADERS))

$(OBJ)/%.o: $(SRC)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(SHADERS_OBJ)/%.spv: $(SHADERS_SRC)/%
	$(VULKAN_SDK)\Bin32\glslangValidator -V -o $@ $^

$(OUT): $(OBJECTS) $(SHADER_OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(CFLAGS) $(LINKFLAGS)

help:
	@echo sources = $(SOURCES)
	@echo objects = $(OBJECTS)
	@echo shaders = $(SHADERS)
	@echo shaders_objects = $(SHADER_OBJECTS)
	@echo command = $(CC) $(CFLAGS) $(LINKFLAGS) $(SOURCES)

clean:
	-del /q $(OBJ)\*.o
	-del /q $(SHADERS_OBJ)\*.spv
	-del *.exe


