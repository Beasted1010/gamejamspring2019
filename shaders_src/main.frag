#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (std140, binding = 0) uniform model_vals {
    mat4 model;
} model_buffer;

layout (binding = 2) uniform sampler2D tex_sampler;

layout (location = 0) in vec4 pos;
layout (location = 1) in vec2 coord;
layout (location = 2) in vec4 normal;

layout (location = 0) out vec4 outColor;

void main() {
   outColor = texture( tex_sampler, coord );
}

