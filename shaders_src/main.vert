#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (std140, binding = 0) uniform model_vals {
    mat4 model;
} model_buffer;

layout (std140, binding = 1) uniform view_vals {
    mat4 vp;
} view_buffer;

layout (location = 0) in vec4 in_pos;
layout (location = 1) in vec2 in_coord;
layout (location = 2) in vec4 in_normal;

layout (location = 0) out vec4 out_pos;
layout (location = 1) out vec2 out_coord;
layout (location = 2) out vec4 out_normal;

void main() {
    out_pos = in_pos;
    out_coord = in_coord;
    out_normal = in_normal;
    
    gl_Position = view_buffer.vp * model_buffer.model * in_pos;
}

