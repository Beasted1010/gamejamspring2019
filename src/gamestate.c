
#include <time.h>
#include <stdlib.h>
#include <math.h>

#include "gamestate.h"

#include "vulkan_wrapper.h"
#include "common.h"

#define PI 3.1415926535

void SpawnPickup( struct Pickup* pickups, uint16_t* pickupCount );

// NOTE: I am assuming everything that moves is a circle, hence MoveEntity uses radius w/ pos validating

void InitializePlayer( struct Player* player )
{
    // TODO: state->rendering

    player->entity.radius = PLAYER_START_RADIUS;
    player->entity.position.x = MAP_MAX_X / 2;
    player->entity.position.y = MAP_MAX_Y / 2;
    player->entity.velocity.x = 0;
    player->entity.velocity.y = 0;
    player->entity.accel.x = 0;
    player->entity.accel.y = 0;
    player->entity.mass = PLAYER_START_MASS;
    player->fire_backward = 0;

    player->perimeter_circle.position.x = player->entity.position.x;
    player->perimeter_circle.position.y = player->entity.position.y;
    player->perimeter_circle.radius = player->entity.radius * PLAYER_PERIMETER_RADIUS_MULTIPLIER;

    player->loadedFireball.entity.mass = 0; // TODO: A better flag for existance than mass...

    player->last_fireball_shot_time_elapse = 0;

    // TODO: player->model
    
    player->score = 0;
    player->kill_count = 0;
    
    player->direction = PLAYER_START_DIRECTION;
    player->strength = 0;
}

void InitializeMapData( struct MapData* mapData )
{
    mapData->rect.min.x = 0;
    mapData->rect.min.y = 0;

    mapData->rect.max.x = MAP_MAX_X;
    mapData->rect.max.y = MAP_MAX_Y;
}

void InitializeInput( struct Input* input )
{
    input->keydown[KeyFireBackwards] = 0;
    input->mousestate = 0;
    input->mousex = input->mousey = 0;
    input->pause = 0;
    input->quit = 0;
}

void InitializeFrameData( struct GameState* state )
{
    state->logical_frames = 0;
    state->fps = 0;

    state->pickup_spawn_time_elapsed = 0;
    state->spawn_time_elapsed = 0;
    state->difficulty_jump_time_elapsed = 0;
}

void InitializeGameData( struct GameState* state )
{
    state->current_enemy_count = 0;
    state->current_fireball_count = 0;
    state->current_pickup_count = 0;
    state->difficulty = START_ENEMY_SPAWN_RATE;
    state->last_enemy_spawn_time = 0;
    state->playing = 1;
}

void InitilaizePickups( struct Pickup* pickups, uint16_t* pickupCount )
{
    for( int i = 0; i < INITIAL_PICKUP_COUNT; i++ )
    {
        SpawnPickup( pickups, pickupCount );
    }
}

void CreateGameState( struct GameState* state )
{
    LOG_CHECKPOINT("Creating GameState");
    srand(time(0));

    InitializePlayer( &state->player );

    InitializeMapData( &state->map_data );

    InitializeInput( &state->input );

    InitializeFrameData( state );
    InitializeGameData( state );

    InitilaizePickups( state->pickups, &state->current_pickup_count );

    LOG_CHECKPOINT("GameState Created");
}

int CirclesCollided( struct FloatCircle circle1, struct FloatCircle circle2 )
{
    Vector2 circles_distance_vector;
    circles_distance_vector.x = circle1.position.x - circle2.position.x;
    circles_distance_vector.y = circle1.position.y - circle2.position.y;

    /*if( circles_distance_vector.x < 0 )
        circles_distance_vector.x *= -1;

    if( circles_distance_vector.y < 0 )
        circles_distance_vector.y *= -1;*/

    float circles_distance_magnitude = sqrt( circles_distance_vector.x * circles_distance_vector.x + 
                                             circles_distance_vector.y * circles_distance_vector.y );

    if( circles_distance_magnitude < circle1.radius + circle2.radius )
    {
        return 1;
    }

    return 0;
}

int EntitiesCollided( struct Entity* entity1, struct Entity* entity2 )
{
    return CirclesCollided( (struct FloatCircle) { entity1->position, entity1->radius }, 
                            (struct FloatCircle) { entity2->position, entity2->radius } );
}

void ValidateVelocity( Vector2* velocity )
{
    if( velocity->x > MAX_VELOCITY )
        velocity->x = MAX_VELOCITY;
    else if( velocity->x < -MAX_VELOCITY )
        velocity->x = -MAX_VELOCITY;

    if( velocity->y > MAX_VELOCITY )
        velocity->y = MAX_VELOCITY;
    else if( velocity->y < -MAX_VELOCITY )
        velocity->y = -MAX_VELOCITY;
}

void ValidateAcceleration( Vector2* acceleration )
{
    if( acceleration->x > MAX_ACCEL )
        acceleration->x = MAX_ACCEL;
    else if( acceleration->x < -MAX_ACCEL )
        acceleration->x = -MAX_ACCEL;

    if( acceleration->y > MAX_ACCEL )
        acceleration->y = MAX_ACCEL;
    else if( acceleration->y < -MAX_ACCEL )
        acceleration->y = -MAX_ACCEL;
}

void ValidatePosition( Vector2* position, float radius, Vector2* velocity, struct FloatRect* rect )
{
    if( position->x + radius > rect->max.x )
    {
        velocity->x *= -1;
        position->x -= ( (position->x + radius) - rect->max.x );
    }
    else if( position->x - radius < rect->min.x )
    {
        velocity->x *= -1;
        position->x += -( position->x - radius );
    }

    if( position->y + radius > rect->max.y )
    {
        velocity->y *= -1;
        position->y -= ( (position->y + radius) - rect->max.x );
    }
    else if( position->y - radius < rect->min.y )
    {
        velocity->y *= -1;
        position->y += -( position->y - radius );
    }
}

void ValidateMass( float* mass )
{
    if( *mass < 0 )
        *mass = 0;
}

void UpdateScore( uint16_t* score, int change )
{
    if( *score + change < 0 )
        *score = 0;
    else
        *score += change;
}

void MoveEntity( struct Entity* entity, float deltaTime, struct FloatRect* boundingRect )
{
    //entity->position.x = entity->position.x + entity->velocity.x * deltaTime + 0.5 * CONSTANT_ACCELERATION * deltaTime * deltaTime;
    //entity->position.y = entity->position.y + entity->velocity.y * deltaTime + 0.5 * CONSTANT_ACCELERATION * deltaTime * deltaTime;
    
    entity->velocity.x = entity->velocity.x + entity->accel.x * deltaTime;
    entity->velocity.y = entity->velocity.y + entity->accel.y * deltaTime;

    entity->position.x = entity->position.x + entity->velocity.x * deltaTime + 0.5 * entity->accel.x * deltaTime * deltaTime;
    entity->position.y = entity->position.y + entity->velocity.y * deltaTime + 0.5 * entity->accel.y * deltaTime * deltaTime;

    if( entity->velocity.x )
    {
        // APPLY SLOWING DOWN - Tend towards 0
        float dv = FRICTION * deltaTime * entity->mass;
        // If velocity crosses 0
        if( (entity->velocity.x > 0 && entity->velocity.x - dv < 0) || 
            (entity->velocity.x < 0 && entity->velocity.x + dv > 0) )
            entity->velocity.x = 0;
        else
        {
            if( entity->velocity.x < 0 )
                entity->velocity.x += dv;
            else
                entity->velocity.x -= dv;
        }
    }

    if( entity->velocity.y )
    {
        // APPLY SLOWING DOWN - Tend towards 0
        float dv = FRICTION * deltaTime * entity->mass;
        // If velocity crosses 0
        if( (entity->velocity.y > 0 && entity->velocity.y - dv < 0) || 
            (entity->velocity.y < 0 && entity->velocity.y + dv > 0) )
            entity->velocity.y = 0;
        else
        {
            if( entity->velocity.y < 0 )
                entity->velocity.y += dv;
            else
                entity->velocity.y -= dv;
        }
    }

    if( entity->accel.x )
    {
        // APPLY SLOWING DOWN - Tend towards 0
        float da = FRICTION * deltaTime * entity->mass;
        // If velocity crosses 0
        if( (entity->accel.x > 0 && entity->accel.x - da < 0) || 
            (entity->accel.x < 0 && entity->accel.x + da > 0) )
            entity->accel.x = 0;
        else
        {
            if( entity->accel.x < 0 )
                entity->accel.x += da;
            else
                entity->accel.x -= da;
        }
    }

    if( entity->accel.y )
    {
        // APPLY SLOWING DOWN - Tend towards 0
        float da = FRICTION * deltaTime * entity->mass;
        // If accel crosses 0
        if( (entity->accel.y > 0 && entity->accel.y - da < 0) || 
            (entity->accel.y < 0 && entity->accel.y + da > 0) )
            entity->accel.y = 0;
        else
        {
            if( entity->accel.y < 0 )
                entity->accel.y += da;
            else
                entity->accel.y -= da;
        }
    }

    /*entity->velocity.x /= entity->mass * MASS_IMPACT_TO_SPEED_FACTOR;
    entity->velocity.y /= entity->mass * MASS_IMPACT_TO_SPEED_FACTOR;*/

    ValidateVelocity( &entity->velocity );
    ValidateAcceleration( &entity->accel );
    ValidatePosition( &entity->position, entity->radius, &entity->velocity, boundingRect );
}

void CreateEnemy( struct Enemy* enemy )
{
    enemy->entity.radius = ENEMY_START_RADIUS; // TODO: RANDOM
    enemy->movement_delay_time_elapsed = 0;

    // 50/50 chance, a top/bottom edge or left/right?
    if( rand() % 2 )
    {
        // Top or bottom?
        if( rand() % 2 )
            enemy->entity.position.y = 0;
        else
            enemy->entity.position.y = MAP_MAX_Y - enemy->entity.radius;

        enemy->entity.position.x = (rand() % MAP_MAX_X) + 1;
    }
    else
    {
        // Left or right?
        if( rand() % 2 )
            enemy->entity.position.x = 0;
        else
            enemy->entity.position.x = MAP_MAX_X - enemy->entity.radius;

        enemy->entity.position.y = (rand() % MAP_MAX_Y) + 1;
    }

    if( enemy->entity.position.x + enemy->entity.radius > MAP_MAX_X )
        enemy->entity.position.x -= ( (enemy->entity.position.x + enemy->entity.radius) - MAP_MAX_X );
    else if( enemy->entity.position.x - enemy->entity.radius < 0 )
        enemy->entity.position.x += -( enemy->entity.position.x - enemy->entity.radius );

    if( enemy->entity.position.y + enemy->entity.radius > MAP_MAX_Y )
        enemy->entity.position.y -= ( (enemy->entity.position.y + enemy->entity.radius) - MAP_MAX_Y );
    else if( enemy->entity.position.y - enemy->entity.radius < 0 )
        enemy->entity.position.y += -( enemy->entity.position.y - enemy->entity.radius );

    enemy->entity.velocity.x = INITIAL_SPEED;
    enemy->entity.velocity.y = INITIAL_SPEED;
    enemy->entity.accel.x = INITIAL_ACCELERATION;
    enemy->entity.accel.y = INITIAL_ACCELERATION;

    // Ignore the NONE option
    enemy->personality = ( rand() % (PERSONALITY_COUNT - 1) ) + 1;

    enemy->time_elapsed = 0;

    switch( enemy->personality )
    {
        case HEADHUNTER:
        {
        } break;

        case TEASER:
        {
            enemy->mode = TEASE_MODE;
        } break;

        case NARROWSIGHT:
        {
            enemy->mode = PLAN_COURSE_MODE;
        } break;

        case ROBBER:
        {
            enemy->mode = THIEF_MODE;
        } break;

        case TERRORIST:
        {
            enemy->mode = SUICIDE_MODE;
        } break;

        default:
        {
            enemy->mode = IDLE_MODE;
        } break;
    }

    // TODO: enemy->model
    
    enemy->entity.mass = enemy->entity.radius; // TODO: RANDOM
}

void DestroyEnemy( struct Enemy* enemies, uint32_t* enemyCount, uint32_t enemyIndex )
{
    if( enemyIndex >= *enemyCount )
    {
        return;
    }

    enemies[enemyIndex] = enemies[ --(*enemyCount) ];
}

void SpawnEnemy( struct Enemy* enemies, uint32_t* enemyCount )
{
    if( *enemyCount >= MAX_ENEMY_COUNT )
        return;

    CreateEnemy( &enemies[ (*enemyCount)++ ] );

    LOG_INFO("Spawned enemy with personality #%i! Current count: %i\n", enemies[(*enemyCount)-1].personality,
                                                                        *enemyCount);
}

static inline void GetVectorToTarget( Vector2* vectorOut, Vector2* targetPosition, Vector2* entityPosition )
{
    vectorOut->x = targetPosition->x - entityPosition->x;
    vectorOut->y = targetPosition->y - entityPosition->y;
}

static inline int EntityInsideCircle( struct Entity* entity, struct FloatCircle* circle )
{
    if( CirclesCollided( (struct FloatCircle) { entity->position, entity->radius }, *circle ) )
        return 1;

    return 0;
}

static inline void SetCourse( struct Enemy* enemy,
                              float playerDirection, struct FloatCircle* playerPerimeterCircle,
                              Vector2* moveDirectionVectorOut )
{
    Vector2 perimeterCirclePoint;
    perimeterCirclePoint.x = playerPerimeterCircle->position.x + 
                             playerPerimeterCircle->radius * cos( playerDirection );
    perimeterCirclePoint.y = playerPerimeterCircle->position.y + 
                             playerPerimeterCircle->radius * sin( playerDirection );

    GetVectorToTarget( moveDirectionVectorOut, &perimeterCirclePoint, &enemy->entity.position );
}

static inline void FindNearestPickup( struct Pickup* pickups, uint16_t* pickupCount, 
                                      Vector2* entityPosition, Vector2* vectorToNearestPickup )
{
    Vector2 vector_to_target;

    GetVectorToTarget( vectorToNearestPickup, &pickups[0].circle_body.position, entityPosition );
    float shortest_distance_to_pickup = sqrt( vectorToNearestPickup->x * vectorToNearestPickup->x + 
                                                vectorToNearestPickup->y * vectorToNearestPickup->y );

    for( int i = 0; i < *pickupCount; i++ )
    {
        GetVectorToTarget( &vector_to_target, &pickups[i].circle_body.position, entityPosition );
        float distance_to_pickup = sqrt( vector_to_target.x * vector_to_target.x + 
                                           vector_to_target.y * vector_to_target.y );

        if( distance_to_pickup < shortest_distance_to_pickup )
        {
            *vectorToNearestPickup = vector_to_target;
            shortest_distance_to_pickup = distance_to_pickup;
        }
    }
}

static inline void FindNearestFireball( struct Fireball* fireballs, uint16_t* fireballCount, 
                                        Vector2* entityPosition, Vector2* vectorToNearestFireball )
{
    Vector2 vector_to_target;

    GetVectorToTarget( vectorToNearestFireball, &fireballs[0].entity.position, entityPosition );
    float shortest_distance_to_fireball = sqrt( vectorToNearestFireball->x * vectorToNearestFireball->x + 
                                                vectorToNearestFireball->y * vectorToNearestFireball->y );

    for( int i = 0; i < *fireballCount; i++ )
    {
        GetVectorToTarget( &vector_to_target, &fireballs[i].entity.position, entityPosition );
        float distance_to_fireball = sqrt( vector_to_target.x * vector_to_target.x + 
                                           vector_to_target.y * vector_to_target.y );

        if( distance_to_fireball < shortest_distance_to_fireball )
        {
            *vectorToNearestFireball = vector_to_target;
            shortest_distance_to_fireball = distance_to_fireball;
        }
    }
}

void UpdateEnemy( struct Enemy* enemy, float deltaTime, struct FloatRect* boundingRect, struct Player* player, 
                  struct Fireball* fireballs, uint16_t* fireballCount,
                  struct Pickup* pickups, uint16_t* pickupCount )
{
    enemy->movement_delay_time_elapsed += deltaTime;

    if( enemy->movement_delay_time_elapsed >= ENEMY_MOVE_DELAY )
    {
        enemy->movement_delay_time_elapsed = 0;

        Vector2 move_direction_vector;
        GetVectorToTarget( &move_direction_vector, &player->entity.position, &enemy->entity.position );

        switch( enemy->personality )
        {
            case HEADHUNTER:
            {
                GetVectorToTarget( &move_direction_vector, &player->entity.position, &enemy->entity.position );
            } break;

            case TEASER:
            {
                GetVectorToTarget( &move_direction_vector, &player->entity.position, &enemy->entity.position );
                
                if( enemy->mode == RETREAT_MODE )
                {
                    move_direction_vector.x *= -1;
                    move_direction_vector.y *= -1;
                }

                enemy->time_elapsed += deltaTime;

                uint8_t entity_in_perimeter = EntityInsideCircle( &enemy->entity, &player->perimeter_circle );
                if( entity_in_perimeter && enemy->mode == TEASE_MODE && enemy->time_elapsed >= TEASE_TIMEOUT_SECONDS )
                {
                    enemy->mode = RETREAT_MODE;

                    enemy->time_elapsed = 0;
                }
                else if( !entity_in_perimeter && enemy->mode == RETREAT_MODE && 
                         enemy->time_elapsed >= RETREAT_TIMEOUT_SECONDS )
                {
                    enemy->mode = TEASE_MODE;

                    enemy->time_elapsed = 0;
                }
            } break;

            case NARROWSIGHT: // TODO TODO This could use more tweaking
            {
                enemy->time_elapsed += deltaTime;

                // If we are executing, then grab the current velocity's direction as the new direction
                if( enemy->mode == EXECUTE_COURSE_MODE && enemy->time_elapsed < COURSE_TIMEOUT_SECONDS )
                {
                    move_direction_vector.x = enemy->entity.velocity.x;
                    move_direction_vector.y = enemy->entity.velocity.y;
                }

                if( enemy->mode == PLAN_COURSE_MODE && enemy->time_elapsed >= COURSE_RECOVERY_TIMEOUT_SECONDS )
                {
                    // Populates "move_direction_vector"
                    SetCourse( enemy, player->direction, 
                               &player->perimeter_circle, &move_direction_vector );

                    enemy->time_elapsed = 0;
                    enemy->mode = EXECUTE_COURSE_MODE;
                }
                else if( enemy->mode == EXECUTE_COURSE_MODE && enemy->time_elapsed >= COURSE_TIMEOUT_SECONDS )
                {
                    GetVectorToTarget( &move_direction_vector, &player->entity.position, &enemy->entity.position );

                    enemy->time_elapsed = 0;
                    enemy->mode = PLAN_COURSE_MODE;
                }
            } break;

            case ROBBER:
            {
                if( *pickupCount == 0 )
                {
                    enemy->mode = IDLE_MODE;
                    GetVectorToTarget( &move_direction_vector, &player->entity.position, &enemy->entity.position );
                }
                else
                {
                    enemy->mode = THIEF_MODE;
                    FindNearestPickup( pickups, pickupCount, &enemy->entity.position, &move_direction_vector );
                }
            } break;

            case TERRORIST:
            {
                if( *fireballCount == 0 )
                {
                    enemy->mode = IDLE_MODE;
                    GetVectorToTarget( &move_direction_vector, &player->entity.position, &enemy->entity.position );
                }
                else
                {
                    enemy->mode = THIEF_MODE;
                    FindNearestFireball( fireballs, fireballCount, &enemy->entity.position, &move_direction_vector );
                }
            } break;

            default:
            {
            } break;
        }

        // TODO: Something other than a constant speed so friction can have an effect?
        //enemy->entity.velocity.x = ( move_direction_vector.x > 0 ? 1 : -1 ) * ENEMY_SPEED;
        //enemy->entity.velocity.y = ( move_direction_vector.y > 0 ? 1 : -1 ) * ENEMY_SPEED;

        enemy->entity.accel.x = ( move_direction_vector.x > 0 ? CONSTANT_ACCELERATION : -CONSTANT_ACCELERATION );// * ENEMY_SPEED;
        enemy->entity.accel.y = ( move_direction_vector.y > 0 ? CONSTANT_ACCELERATION : -CONSTANT_ACCELERATION );// * ENEMY_SPEED;
        ValidateAcceleration( &enemy->entity.accel );
    }

    MoveEntity( &enemy->entity, deltaTime, boundingRect );
}

void UpdateEnemies( struct Enemy* enemies, uint32_t* enemyCount, float deltaTime, 
                    struct FloatRect* boundingRect, struct Player* player,
                    struct Fireball* fireballs, uint16_t* fireballCount,
                    struct Pickup* pickups, uint16_t* pickupCount )
{
    for( int i = 0; i < *enemyCount; i++ )
    {
        UpdateEnemy( &enemies[i], deltaTime, boundingRect, player, 
                     fireballs, fireballCount, pickups, pickupCount );
    }
}

void CreatePickup( struct Pickup* pickup )
{
    pickup->type = ( rand() % (PICKUP_TYPE_COUNT - 1) ) + 1;

    switch( pickup->type )
    {
        case ICE_CUBE_PICKUP:
        {
            pickup->circle_body.radius = ICE_CUBE_PICKUP_RADIUS;
        } break;

        default:
        {
        } break;
    }

    pickup->circle_body.position.x = rand() % MAP_MAX_X;
    pickup->circle_body.position.y = rand() % MAP_MAX_Y;
}

void SpawnPickup( struct Pickup* pickups, uint16_t* pickupCount )
{
    if( *pickupCount >= MAX_PICKUP_COUNT )
        return;

    CreatePickup( &pickups[ (*pickupCount)++ ] );

    LOG_INFO("Spawned pickup with type #%i! Current count: %i\n", pickups[(*pickupCount)-1].type, *pickupCount);
}

void DeletePickup( struct Pickup* pickups, uint16_t* pickupCount, uint16_t pickupIndex )
{
    if( pickupIndex >= *pickupCount )
    {
        return;
    }

    pickups[pickupIndex] = pickups[ --(*pickupCount) ];
}

void CreateFireball( struct Fireball* fireball, Vector2* position )
{
    fireball->entity.position.x = position->x;
    fireball->entity.position.y = position->y;

    fireball->entity.velocity.x = 0;
    fireball->entity.velocity.y = 0;

    fireball->entity.radius = FIREBALL_START_RADIUS;
    fireball->entity.mass = fireball->entity.radius;

    // TODO: fireball->model = ;

    fireball->seconds_existed = 0;
}

void DestroyFireball( struct Fireball* fireballs, uint16_t* fireballCount, uint16_t fireballIndex )
{
    if( fireballIndex >= *fireballCount )
    {
        return;
    }

    fireballs[fireballIndex] = fireballs[ --(*fireballCount) ];
}

void SpawnFireball( struct Fireball* fireballs, uint16_t* fireballCount,
                    Vector2* position )
{
    if( *fireballCount >= MAX_FIREBALL_COUNT )
    {
        DestroyFireball( fireballs, fireballCount, *fireballCount - 1 );
    }

    CreateFireball( &fireballs[ (*fireballCount)++ ], position );

    LOG_INFO("Spawned fireball! Current count: %i", *fireballCount);
}

void AddFireballToFireballs( struct Fireball* fireball, 
                             struct Fireball* fireballs, uint16_t* fireballCount )
{
    fireballs[ (*fireballCount)++ ] = *fireball;
}

void UpdateFireballs( struct Fireball* fireballs, uint16_t* fireballCount, float deltaTime,
                      struct FloatRect* boundingRect )
{
    for( int i = 0; i < *fireballCount; i++ )
    {
        fireballs[i].entity.position.x = 
            fireballs[i].entity.position.x + fireballs[i].entity.velocity.x * deltaTime;
        fireballs[i].entity.position.y = 
            fireballs[i].entity.position.y + fireballs[i].entity.velocity.y * deltaTime;

        if( fireballs[i].entity.position.x + fireballs[i].entity.radius > boundingRect->max.x ||
            fireballs[i].entity.position.y + fireballs[i].entity.radius > boundingRect->max.y ||
            fireballs[i].entity.position.x - fireballs[i].entity.radius < boundingRect->min.x ||
            fireballs[i].entity.position.y - fireballs[i].entity.radius < boundingRect->min.y )
        {
            DestroyFireball( fireballs, fireballCount, i );
            continue;
        }

        fireballs[i].seconds_existed += deltaTime;

        if( fireballs[i].seconds_existed > FIREBALL_EXISTANCE_SECONDS )
        {
            DestroyFireball( fireballs, fireballCount, i );
        }
    }
}

void LaunchFireball( struct Player* player, struct Input* input,
                     struct Fireball* fireballs, uint16_t* fireballCount )
{
    Vector2 fireball_rel_velocity;
    fireball_rel_velocity.x = player->strength * cos( player->direction );
    fireball_rel_velocity.y = player->strength * sin( player->direction );

    if ( player->fire_backward )
    {
        fireball_rel_velocity.x *= -1;
        fireball_rel_velocity.y *= -1;
    }

    float velocity_factor = 1;
    if ( player->entity.mass != 0 )
    {
        velocity_factor = player->loadedFireball.entity.mass / player->entity.mass;
    }
    
    Vector2 new_player_velocity;
    new_player_velocity.x = player->entity.velocity.x - velocity_factor * fireball_rel_velocity.x;
    new_player_velocity.y = player->entity.velocity.y - velocity_factor * fireball_rel_velocity.y;

    player->loadedFireball.entity.velocity.x = player->entity.velocity.x + fireball_rel_velocity.x;
    player->loadedFireball.entity.velocity.y = player->entity.velocity.y + fireball_rel_velocity.y;

    AddFireballToFireballs( &player->loadedFireball, fireballs, fireballCount );

    player->entity.velocity = new_player_velocity;

    UpdateScore( &player->score, -(LAUNCH_FIREBALL_PENALTY_MULTIPLIER * player->strength) );
    player->strength = 0;

    // Fireball no longer equipped on player, so loadedFireball has no mass. This serves as flag to existance
    player->loadedFireball.entity.mass = 0;
}

void UpdatePlayer( struct Player* player, struct Input* input, float deltaTime, 
                   struct Fireball* fireballs, uint16_t* fireballCount,
                   struct FloatRect* boundingRect )
{
    player->last_fireball_shot_time_elapse += deltaTime;

    if( player->last_fireball_shot_time_elapse >= FIREBALL_SHOT_DELAY_SECONDS || player->loadedFireball.entity.mass != 0 )
    {
        player->last_fireball_shot_time_elapse = 0;

        if( input->mousestate == MOUSE_LDOWN )
        {
            if( !player->loadedFireball.entity.mass )
            {
                player->fire_backward = input->keydown[KeyFireBackwards];
                CreateFireball( &player->loadedFireball, &player->entity.position );
            }
            else
            {
                // TODO: Add arming animation while strength builds up?
                player->loadedFireball.entity.position.x = player->entity.position.x;
                player->loadedFireball.entity.position.y = player->entity.position.y;
            }

            if( player->strength < MAX_STRENGTH )
                player->strength += STRENGTH_INCREASE * deltaTime;
            else
                player->strength = MAX_STRENGTH;
        }
        else
        {
            // If player has built up some strength from holding down mouse, but mouse isn't down anymore, apply the shot
            if( player->strength && player->loadedFireball.entity.mass )
            {
                LaunchFireball( player, input, fireballs, fireballCount );
            }
        }
    }

    player->direction -= input->mousex * PLAYER_TURN_SPEED;

    MoveEntity( &player->entity, deltaTime, boundingRect );

    player->perimeter_circle.position.x = player->entity.position.x;
    player->perimeter_circle.position.y = player->entity.position.y;
}

void CheckPlayerEnemyContact( struct Enemy* enemies, uint32_t* enemyCount, 
                              struct Player* player )
{
    for( int i = 0; i < *enemyCount; i++ )
    {
        if( EntitiesCollided( &player->entity, &enemies[i].entity ) )
        {
            player->entity.mass -= PLAYER_HIT_MASS_LOSS;
            //player->entity.radius -= PLAYER_HIT_RADIUS_LOSS;

            DestroyEnemy( enemies, enemyCount, i );

            if( player->entity.mass < 0 )
            {
                player->entity.mass = 0;
            }

            player->entity.radius = player->entity.mass;
            UpdateScore( &player->score, -PLAYER_HIT_PENALTY );
        }
    }
}

void CheckEnemyEnemyContact( struct Enemy* enemies, uint32_t enemyCount )
{
    for( int i = 0; i < enemyCount; i++ )
    {
        for( int j = i; j < enemyCount; j++ )
        {
            if( EntitiesCollided( &enemies[i].entity, &enemies[j].entity ) )
            {
                float allowed_distance = enemies[i].entity.radius + enemies[j].entity.radius;
                float current_distance_x = enemies[i].entity.position.x - enemies[j].entity.position.x;
                float current_distance_y = enemies[i].entity.position.y - enemies[j].entity.position.y;

                float current_distance = sqrt( current_distance_x * current_distance_x + 
                                               current_distance_y * current_distance_y );

                float entity_1_dist_from_mid_x = (MAP_MAX_X / 2) - enemies[i].entity.position.x;
                float entity_2_dist_from_mid_x = (MAP_MAX_X / 2) - enemies[j].entity.position.x;

                float entity_1_dist_from_mid_y = (MAP_MAX_Y / 2) - enemies[i].entity.position.y;
                float entity_2_dist_from_mid_y = (MAP_MAX_Y / 2) - enemies[j].entity.position.y;

                float entity_1_dist_from_mid = sqrt( entity_1_dist_from_mid_x * entity_1_dist_from_mid_x +
                                                     entity_1_dist_from_mid_y * entity_1_dist_from_mid_y );
                float entity_2_dist_from_mid = sqrt( entity_2_dist_from_mid_x * entity_2_dist_from_mid_x +
                                                     entity_2_dist_from_mid_y * entity_2_dist_from_mid_y );

                // Entity 1 is closer to middle
                if( abs(entity_1_dist_from_mid) < abs(entity_2_dist_from_mid) )
                {
                    // If entity 1 is left of middle, then move to right
                    if( entity_1_dist_from_mid_x < 0 )
                        enemies[i].entity.position.x -= allowed_distance - current_distance;
                    else
                        enemies[i].entity.position.x += allowed_distance - current_distance;

                    if( entity_1_dist_from_mid_y < 0 )
                        enemies[i].entity.position.y -= allowed_distance - current_distance;
                    else
                        enemies[i].entity.position.y += allowed_distance - current_distance;
                }
                else
                {
                    if( entity_2_dist_from_mid_x < 0 )
                        enemies[j].entity.position.x -= allowed_distance - current_distance;
                    else
                        enemies[j].entity.position.x += allowed_distance - current_distance;

                    if( entity_2_dist_from_mid_y < 0 )
                        enemies[j].entity.position.y -= allowed_distance - current_distance;
                    else
                        enemies[j].entity.position.y += allowed_distance - current_distance;
                }
            }
        }
    }
}

void CheckFireballEnemyContact( struct Enemy* enemies, uint32_t* enemyCount,
                                struct Fireball* fireballs, uint16_t* fireballCount,
                                uint16_t* score, uint32_t* killCount )
{
    for( int i = 0; i < *fireballCount; i++ )
    {
        for( int j = 0; j < *enemyCount; j++ )
        {
            if( CirclesCollided( (struct FloatCircle) { fireballs[i].entity.position, fireballs[i].entity.radius }, 
                                 (struct FloatCircle) { enemies[j].entity.position, enemies[j].entity.radius } ) )
            {
                DestroyFireball( fireballs, fireballCount, i );
                DestroyEnemy( enemies, enemyCount, j );

                (*killCount)++;
                UpdateScore( score, ENEMY_KILL_BOOST );
            }
        }
    }
}

void CheckPlayerPickupContact( struct Player* player, struct Pickup* pickups, uint16_t* pickupCount )
{
    for( int i = 0; i < *pickupCount; i++ )
    {
        if( CirclesCollided( (struct FloatCircle) { player->entity.position, player->entity.radius },
                             pickups[i].circle_body ) )
        {
            // Handle Player interaction with pickup
            switch( pickups[i].type )
            {
                case ICE_CUBE_PICKUP:
                {
                    player->entity.mass += ICE_CUBE_PICKUP_MASS_BOOST;

                    if( player->entity.mass > MAX_PLAYER_MASS )
                        player->entity.mass = MAX_PLAYER_MASS;

                    player->entity.radius = player->entity.mass;
                } break;

                default:
                {
                } break;
            }

            DeletePickup( pickups, pickupCount, i );
        }
    }
}

void CheckEnemyPickupContact( struct Enemy* enemies, uint32_t enemyCount, 
                              struct Pickup* pickups, uint16_t* pickupCount )
{
    for( int i = 0; i < enemyCount; i++ )
    {
        for( int j = 0; j < *pickupCount; j++ )
        {
            if( CirclesCollided( (struct FloatCircle) { enemies[i].entity.position, enemies[i].entity.radius },
                                 pickups[j].circle_body ) )
            {
                // Handle Enemy interaction with pickup
                switch( pickups[i].type )
                {
                    // Nothing right now (: just delete 'er
                    case ICE_CUBE_PICKUP:
                    {
                    } break;

                    default:
                    {
                    } break;
                }

                DeletePickup( pickups, pickupCount, j );
            }
        }
    }
}

/*void CheckEntityObjectContact( struct Entity* entity, struct Object* object )
{
}*/

void UpdateGameState( struct GameState* state, float deltaTime )
{
    static float seconds_elapse_time_score_boost = 0;
    seconds_elapse_time_score_boost += deltaTime;

    state->pickup_spawn_time_elapsed += deltaTime;
    state->difficulty_jump_time_elapsed += deltaTime;
    state->spawn_time_elapsed += deltaTime;
    
    if( seconds_elapse_time_score_boost >= SECONDS_ELAPSE_FOR_DIFFICULTY_SCORE_BOOST )
    {
        UpdateScore( &state->player.score, TIME_ELAPSE_SCORE_BOOST_MULTIPLIER * state->difficulty );
    
        seconds_elapse_time_score_boost = 0;
    }

    if( state->pickup_spawn_time_elapsed >= SECONDS_ELAPSE_FOR_PICKUP_SPAWN )
    {
        SpawnPickup( state->pickups, &state->current_pickup_count );

        state->pickup_spawn_time_elapsed = 0;
    }

    if( state->difficulty_jump_time_elapsed >= SECONDS_ELAPSE_FOR_DIFFICULTY_JUMP )
    {
        state->difficulty += DIFFICULTY_INCREASE;

        if( state->difficulty > MAX_DIFFICULTY )
            state->difficulty = MAX_DIFFICULTY;

        state->difficulty_jump_time_elapsed = 0;
    }

    if( state->spawn_time_elapsed >= ( 1 / state->difficulty ) )
    {
        SpawnEnemy( state->enemies, &state->current_enemy_count );

        state->spawn_time_elapsed = 0;
    }

    UpdateEnemies( state->enemies, &state->current_enemy_count, deltaTime, 
                   &state->map_data.rect, &state->player,
                   state->fireballs, &state->current_fireball_count,
                   state->pickups, &state->current_pickup_count );
    UpdatePlayer( &state->player, &state->input, deltaTime, 
                  state->fireballs, &state->current_fireball_count,
                  &state->map_data.rect );
    UpdateFireballs( state->fireballs, &state->current_fireball_count, deltaTime, &state->map_data.rect );

    //CheckEnemyEnemyContact( state->enemies, state->current_enemy_count );
    CheckPlayerEnemyContact( state->enemies, &state->current_enemy_count, &state->player );
    CheckFireballEnemyContact( state->enemies, &state->current_enemy_count, 
                               state->fireballs, &state->current_fireball_count,
                               &state->player.score, &state->player.kill_count );
    CheckPlayerPickupContact( &state->player, state->pickups, &state->current_pickup_count );
    CheckEnemyPickupContact( state->enemies, state->current_enemy_count, 
                             state->pickups, &state->current_pickup_count );

    if( state->player.entity.mass <= 0 )
    {
        LOG_INFO("Player mass is zero! Game Over!");
        state->playing = 0;
    }
}


void DestroyGameState( struct GameState* state )
{
    LOG_CHECKPOINT("Destroying GameState");

    state->input.keydown[KeyFireBackwards] = 0;
    state->input.mousestate = 0;
    state->input.mousex = state->input.mousey = 0;
    state->input.pause = 0;
    state->input.quit = 0;

    state->logical_frames = 0;
    state->fps = 0;
    state->playing = 0;

    state->current_enemy_count = 0;
    state->current_fireball_count = 0;
    state->current_pickup_count = 0;
    state->pickup_spawn_time_elapsed = 0;
    state->spawn_time_elapsed = 0;
    state->difficulty_jump_time_elapsed = 0;
    state->last_enemy_spawn_time = 0;

    state->player.score = 0;

    LOG_CHECKPOINT("GameState Destroyed");
}


