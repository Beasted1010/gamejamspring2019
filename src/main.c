#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"

#include "vulkan_wrapper.h"
#include "platform.h"

#include "listmalloc.h"
#include "common.h"
#include "matrix.h"
#include "gamestate.h"
#include "model_data.h"

char error_buf[256];

// This struct contains all vulkan data needed to render a cube.
static struct 
{
    // Vulkan initialization data, roughly in chronological order
    // Everything here is initialized and destroyed once.
    VkApplicationInfo app_info;
    VkInstance inst;

    VkPhysicalDevice* gpus;
    uint32_t gpu_count;

    VkSurfaceKHR window_surface;

    VkQueueFamilyProperties* queue_families;
    uint32_t queue_count;
    uint32_t graphics_queue_index;
    uint32_t present_queue_index;

    VkDevice dev;

    VkCommandPool command_pool;
    VkCommandBuffer command_buffer;

    VulkanSwapchain swapchain;

    VkQueue graphics_queue;
    VkQueue present_queue;

    // Honestly not entirely sure what these are
    // but we have to build them.
    VkDescriptorSetLayout descriptor_layout;
    VkDescriptorPool descriptor_pool;
    VkDescriptorSet descriptor_set;
    VkPipelineLayout pipeline_layout;

    VulkanImage depth_image;

    VkRenderPass render_pass;

    // Same number of these as swapchain images
    VkFramebuffer* framebuffers;

    VulkanShader fragment_shader;
    VulkanShader vertex_shader;
    
    VkPipeline pipeline;
} vk_info;

static WindowInfo* window;
static struct GameState state;

#define MAP_MODEL 0
#define PLAYER_MODEL 1
#define SLING_MODEL 2
#define LOADED_FIREBALL_MODEL 3
#define ENEMY_MODEL 4
#define FIREBALL_MODEL (ENEMY_MODEL + MAX_ENEMY_COUNT)
#define PICKUP_MODEL (FIREBALL_MODEL + MAX_FIREBALL_COUNT)
#define MAX_MODEL_COUNT (PICKUP_MODEL + MAX_PICKUP_COUNT)
static Matrix4 models[MAX_MODEL_COUNT];

void InitializeVulkanData( const WindowInfo* window );
void DestroyVulkanData( );

void InitializeRenderingData( );
void DestroyRenderingData();

Matrix4 ComputeModelFromData( float radius, Vector2 pos )
{
    Matrix4 model = GetScale( radius,
                              radius,
                              radius );

    model = Mult4( GetTranslation( pos.x, pos.y, radius ),
                   model );

    return model;

}

Matrix4 ComputeEntityModel( struct Entity* entity )
{
    return ComputeModelFromData( entity->radius, entity->position );
}

Matrix4 ComputeCircleModel( struct FloatCircle* circle )
{
    return ComputeModelFromData( circle->radius, circle->position );
}

void BindAndDrawRange( uint32_t model_index,
                       uint32_t model_count,
                       VertexBuffer* vertices,
                       VulkanImage* texture )
{
    UpdateSamplerDescriptorSet( vk_info.dev,
                                vk_info.descriptor_set,
                                texture,
                                state.rendering.texture_sampler,
                                2 );
    
    VkDeviceSize offsets[1] = { 0 };
    vkCmdBindVertexBuffers( vk_info.command_buffer,
                            0,
                            1,
                            &vertices->data.buffer,
                            offsets );

    int index;
    for ( index = 0; index < model_count; index++ )
    {
        uint32_t dynamic_offset = (model_index + index) * state.rendering.model_buffer.alignment;
        vkCmdBindDescriptorSets( vk_info.command_buffer,
                                 VK_PIPELINE_BIND_POINT_GRAPHICS,
                                 vk_info.pipeline_layout,
                                 0,
                                 1,
                                 &vk_info.descriptor_set,
                                 1,
                                 &dynamic_offset );

        vkCmdDraw( vk_info.command_buffer, vertices->vertex_count, 1, 0, 0 );
    }
}

void BindAndDraw( uint32_t model_index, VertexBuffer* vertices, VulkanImage* texture )
{
    BindAndDrawRange( model_index, 1, vertices, texture );
}

uint32_t high_score = 0;
uint32_t high_kills = 0;
char score_message[256];

void RenderGameState( struct GameState* state, float delta_time )
{
    RenderRect render_area;
    render_area.x = 0;
    render_area.y = 0;
    render_area.width  = GetWidth( window );
    render_area.height = GetHeight( window );
    
    Vector3 player_pos;
    player_pos.x = state->player.entity.position.x;
    player_pos.y = state->player.entity.position.y;
    player_pos.z = state->player.entity.radius;

    Vector3 unit_backward;
    unit_backward.x = -cos( state->player.direction );
    unit_backward.y = -sin( state->player.direction );
    unit_backward.z = 0;

#define CAMERA_BACK 40
#define CAMERA_UP 16
#define STRENGTH_SCALE 1 / 5.0

    Vector3 camera_pos = VectAdd( player_pos, VectScale( unit_backward, CAMERA_BACK ) );
    camera_pos.z += CAMERA_UP + state->player.entity.radius;

    state->rendering.view = LookAt( camera_pos,
                                    player_pos,
                                    (Vector3) { 0.0f, 0.0f, 1.0f } );

    Matrix4 vp = Mult4( state->rendering.clip,
                        Mult4( state->rendering.projection,
                               state->rendering.view ) );

    WriteToUniformBuffer( vk_info.dev,
                          &state->rendering.view_buffer,
                          sizeof( Matrix4 ),
                          1,
                          &vp );

    models[SLING_MODEL] = GetScale( 1.2f * state->player.entity.radius,
                                    0,
                                    1.2f * state->player.entity.radius );
    Matrix4 player_rotation = GetRotation( 0, 0, state->player.direction - M_PI / 2 );
    models[SLING_MODEL] = Mult4( player_rotation, models[SLING_MODEL] );
    models[SLING_MODEL] = Mult4( GetTranslation( state->player.entity.position.x,
                                                 state->player.entity.position.y,
                                                 state->player.entity.radius * 2.0f ),
                                 models[SLING_MODEL] );

    if ( state->player.loadedFireball.entity.mass > 0 )
    {
        Vector2 fireball_position;
        fireball_position.x = state->player.entity.position.x;
        fireball_position.y = state->player.entity.position.y;
        if ( state->player.fire_backward )
        {
            fireball_position.x -= STRENGTH_SCALE * unit_backward.x * state->player.strength;
            fireball_position.y -= STRENGTH_SCALE * unit_backward.y * state->player.strength;
        }
        else
        {
            fireball_position.x += STRENGTH_SCALE * unit_backward.x * state->player.strength;
            fireball_position.y += STRENGTH_SCALE * unit_backward.y * state->player.strength;
        }
        
        models[LOADED_FIREBALL_MODEL] = GetTranslation( fireball_position.x,
                                                        fireball_position.y,
                                                        state->player.entity.radius * 3.2f );
    }
    
    models[PLAYER_MODEL] = ComputeEntityModel( &state->player.entity );
    int enemy_index;
    for ( enemy_index = 0; enemy_index < state->current_enemy_count; enemy_index++ )
    {
        models[enemy_index + ENEMY_MODEL] = ComputeEntityModel( &state->enemies[enemy_index].entity );
    }

    int fireball_index;
    for ( fireball_index = 0; fireball_index < state->current_fireball_count; fireball_index++ )
    {
        models[fireball_index + FIREBALL_MODEL] =
            ComputeEntityModel( &state->fireballs[fireball_index].entity );
    }

    int pickup_index;
    for ( pickup_index = 0; pickup_index < state->current_pickup_count; pickup_index++ )
    {
        models[pickup_index + PICKUP_MODEL] =
            ComputeCircleModel( &state->pickups[pickup_index].circle_body );
    }
                    
    WriteToUniformBuffer( vk_info.dev,
                          &state->rendering.model_buffer,
                          sizeof( Matrix4 ),
                          MAX_MODEL_COUNT,
                          models );

    UpdateUniformDescriptorSet( vk_info.dev,
                                vk_info.descriptor_set,
                                &state->rendering.model_buffer,
                                0 );
    
    VkSemaphore acquire_sem;
    uint32_t acquired_image;
    AcquireSwapchainImage( vk_info.dev, 
                           &vk_info.swapchain, 
                           &acquired_image, 
                           &acquire_sem );

    BeginRenderRecording( vk_info.dev,
                          vk_info.render_pass,
                          vk_info.framebuffers[acquired_image],
                          vk_info.command_buffer,
                          render_area );

    vkCmdBindPipeline( vk_info.command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vk_info.pipeline );

    VkViewport viewport;
    viewport.x = render_area.x;
    viewport.y = render_area.y;
    viewport.height = render_area.height;
    viewport.width = render_area.width;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    vkCmdSetViewport( vk_info.command_buffer, 0, 1, &viewport );

    VkRect2D scissor;
    scissor.extent.width = render_area.width;
    scissor.extent.height = render_area.height;
    scissor.offset.x = render_area.x;
    scissor.offset.y = render_area.y;
    vkCmdSetScissor( vk_info.command_buffer, 0, 1, &scissor );

    BindAndDraw( MAP_MODEL, &state->rendering.square_vertices, &state->rendering.map_texture );
    BindAndDraw( PLAYER_MODEL, &state->rendering.cube_vertices, &state->rendering.ice_texture );
    BindAndDraw( SLING_MODEL, &state->rendering.sling_vertices, &state->rendering.sling_texture );
    if ( state->player.loadedFireball.entity.mass > 0 )
    {
        BindAndDraw( LOADED_FIREBALL_MODEL,
                     &state->rendering.fireball_vertices,
                     &state->rendering.fireball_texture );
    }
    BindAndDrawRange( ENEMY_MODEL,
                      state->current_enemy_count,
                      &state->rendering.cube_vertices,
                      &state->rendering.enemy_texture );
    BindAndDrawRange( FIREBALL_MODEL,
                      state->current_fireball_count,
                      &state->rendering.fireball_vertices,
                      &state->rendering.fireball_texture );
    BindAndDrawRange( PICKUP_MODEL,
                      state->current_pickup_count,
                      &state->rendering.cube_vertices,
                      &state->rendering.ice_texture );

    UpdateSamplerDescriptorSet( vk_info.dev,
                                vk_info.descriptor_set,
                                &state->rendering.font.image_data,
                                state->rendering.texture_sampler,
                                2 );
    
    UpdateUniformDescriptorSet( vk_info.dev,
                                vk_info.descriptor_set,
                                &state->rendering.font.model_buffer,
                                0 );
    Vector3 camera_fwd = VectNormalize( VectSub( player_pos, camera_pos ) );
    Vector3 camera_right = VectNormalize( VectCross( camera_fwd, (Vector3){0, 0, 1} ) );
    Vector3 camera_up = VectCross( camera_right, camera_fwd );

    Vector3 text_pos;
    text_pos.x = camera_pos.x + 30 * camera_fwd.x - 23 * camera_right.x + 12 * camera_up.x;
    text_pos.y = camera_pos.y + 30 * camera_fwd.y - 23 * camera_right.y + 12 * camera_up.y;
    text_pos.z = camera_pos.z + 30 * camera_fwd.z - 23 * camera_right.z + 12 * camera_up.z;

    const char* append = "";
    if ( !state->playing )
    {
        append = "\n\n\n\t\t\t\t\tGAME OVER\n\t\t\t  PRESS SPACE TO RESTART";
    }
    sprintf( score_message,
             "SCORE\t%5d\t\t\t\t\t\tHIGH SCORE\t%5d\nKILLS\t%5d\t\t\t\t\t\tHIGH KILLS\t%5d%s",
             state->player.score,
             high_score,
             state->player.kill_count,
             high_kills,
             append );
    
    RenderText( vk_info.dev,
                vk_info.command_buffer,
                vk_info.pipeline_layout,
                vk_info.descriptor_set,
                &state->rendering.font,
                score_message,
                1.2,
                &player_rotation,
                text_pos,
                camera_right );

    EndCommandBuffer( vk_info.command_buffer );

    VkFence draw_fence;
    SubmitRenderQueue( vk_info.dev,
                       vk_info.graphics_queue,
                       &vk_info.command_buffer,
                       1,
                       &acquire_sem, 
                       &draw_fence );

    PresentToDisplay( vk_info.dev,
                      vk_info.present_queue,
                      &vk_info.swapchain, 
                      acquired_image,
                      draw_fence );

    vkDestroySemaphore( vk_info.dev, acquire_sem, NULL );
    vkDestroyFence( vk_info.dev, draw_fence, NULL );

    DoWindowRenderUpdate( window );
    
}

int main(int argc, char **argv)
{
    window = InitializeWindowInfo( "Slings Hot!!!", 1280, 720 );
    InitializeVulkanData( window );
    InitializeRenderingData( );

    RenderGameState( &state, 0 );
    
    const uint64_t tickfreq = GetTickFrequency( );

    CreateGameState( &state );
    Matrix4 view_buffer_content =
        Mult4( Mult4( state.rendering.clip, state.rendering.projection ), state.rendering.view );

    WriteToUniformBuffer( vk_info.dev,
                          &state.rendering.view_buffer,
                          sizeof( Matrix4 ),
                          1,
                          &view_buffer_content );
    Vector2 map_center;
    map_center.x = 0.5f * (state.map_data.rect.max.x - state.map_data.rect.min.x);
    map_center.y = 0.5f * (state.map_data.rect.max.y - state.map_data.rect.min.y);

    models[MAP_MODEL] = GetScale( map_center.x,
                          map_center.y,
                          0 );
    models[MAP_MODEL] = Mult4( GetTranslation( map_center.x, map_center.y, 0 ), models[MAP_MODEL] );

    uint64_t last = GetTicks( );
    uint32_t renders_this_second = 0;
    while ( !state.input.quit )
    {
        uint64_t start_ticks = GetTicks( );

        ProcessEvents( &state.input );

        if ( state.input.quit )
        {
            break;
        }

        state.logical_frames++;

        // Enter a rendering loop until
        // enough time has passed to necessitate another logical update
        uint64_t ticks_passed = GetTicks( ) - start_ticks;
        float frames_passed = (ticks_passed * FRAMERATE) / (float)tickfreq;
        while ( frames_passed < 1.0f )
        {
            RenderGameState( &state, ticks_passed / (float) tickfreq );

            uint64_t now = GetTicks( );
            ticks_passed = now - start_ticks;
            frames_passed = (ticks_passed * FRAMERATE) / (float)tickfreq;

            if ( now - last > tickfreq )
            {
                state.fps = renders_this_second;
                renders_this_second = 0;
                last = now;
            }

            renders_this_second++;
        }

        if( state.playing )
        {
            UpdateGameState( &state, ticks_passed / (float) tickfreq );
        }
        else
        {
            if ( state.player.score > high_score )
            {
                high_score = state.player.score;
            }

            if ( state.player.kill_count > high_kills )
            {
                high_kills = state.player.kill_count;
            }
            
            if ( state.input.keydown[KeyFireBackwards] )
            {
                DestroyGameState( &state );
                CreateGameState( &state );
            }
        }
    }

    printf( "Cleaning up...\n" );

    DestroyGameState( &state );
    DestroyRenderingData( );
    DestroyVulkanData( );
    DestroyWindowInfo( window );

    printf( "Done!\n" );

    return 0;
}

void InitializeVulkanData( const WindowInfo* window )
{
    InitializeInstance( &vk_info.app_info, &vk_info.inst, "Test Application" );
    //InitializeDebugReporter( vk_info.inst );
    vk_info.gpus = GetPhysicalDevices( vk_info.inst, &vk_info.gpu_count );
    CreateWindowSurface( vk_info.inst, window, &vk_info.window_surface );
    vk_info.queue_families = GetQueueFamilies( vk_info.gpus[0], 
                                               vk_info.window_surface,
                                               &vk_info.graphics_queue_index,
                                               &vk_info.present_queue_index,
                                               &vk_info.queue_count );

    InitializeLogicalDevice( vk_info.gpus[0], vk_info.graphics_queue_index, &vk_info.dev );
    InitializeCommandPool( vk_info.dev, 
                           vk_info.graphics_queue_index, 
                           &vk_info.command_pool,
                           &vk_info.command_buffer,
                           1 );

    uint32_t width = GetWidth( window );
    uint32_t height = GetHeight( window );
    InitializeSwapchain( vk_info.gpus[0],
                         vk_info.dev,
                         vk_info.window_surface,
                         width,
                         height,
                         vk_info.graphics_queue_index,
                         vk_info.present_queue_index,
                         &vk_info.swapchain );

    InitializeGraphicsAndPresentQueues( vk_info.dev,
                                        vk_info.graphics_queue_index,
                                        vk_info.present_queue_index,
                                        &vk_info.graphics_queue,
                                        &vk_info.present_queue );

    InitializePipelineLayoutAndDescriptorSets( vk_info.dev,
                                               &vk_info.descriptor_layout,
                                               &vk_info.descriptor_pool,
                                               &vk_info.descriptor_set,
                                               &vk_info.pipeline_layout );

    CreateDepthImage( vk_info.gpus[0],
                      vk_info.dev,
                      width,
                      height,
                      &vk_info.depth_image );
    
    InitializeRenderPass( vk_info.dev,
                          &vk_info.depth_image,
                          vk_info.swapchain.format,
                          &vk_info.render_pass );

    vk_info.framebuffers = malloc( sizeof( VkFramebuffer ) * vk_info.swapchain.count );
    InitializeFramebuffers( vk_info.dev,
                            vk_info.render_pass,
                            &vk_info.depth_image,
                            &vk_info.swapchain,
                            width,
                            height,
                            vk_info.framebuffers );

    CreateVertexShader( vk_info.dev,
                        "shaders",
                        "main",
                        &vk_info.vertex_shader );

    CreateFragmentShader( vk_info.dev,
                          "shaders",
                          "main",
                          &vk_info.fragment_shader );

    VulkanShader shaders[] = { vk_info.vertex_shader, vk_info.fragment_shader };

    InitializePipeline( vk_info.dev,
                        vk_info.pipeline_layout,
                        vk_info.render_pass,
                        shaders,
                        SIZE_ARRAY( shaders ),
                        &vk_info.pipeline );

}

void DestroyVulkanData( )
{
    vkDestroyPipeline( vk_info.dev, vk_info.pipeline, NULL );
    FreeShader( vk_info.dev, &vk_info.fragment_shader );
    FreeShader( vk_info.dev, &vk_info.vertex_shader );
    int i;
    for ( i = 0; i < vk_info.swapchain.count; i++ )
    {
        vkDestroyFramebuffer( vk_info.dev, vk_info.framebuffers[i], NULL );
    }
    free( vk_info.framebuffers );
    vkDestroyRenderPass( vk_info.dev, vk_info.render_pass, NULL );
    FreeImage( vk_info.dev, &vk_info.depth_image );
    vkDestroyPipelineLayout( vk_info.dev, vk_info.pipeline_layout, NULL );
    vkFreeDescriptorSets( vk_info.dev, vk_info.descriptor_pool, 1, &vk_info.descriptor_set );
    vkDestroyDescriptorPool( vk_info.dev, vk_info.descriptor_pool, NULL );
    vkDestroyDescriptorSetLayout( vk_info.dev, vk_info.descriptor_layout, NULL );
    for ( i = 0; i < vk_info.swapchain.count; i++ )
    {
        vkDestroyImageView( vk_info.dev, vk_info.swapchain.views[i], NULL );
        vkDestroyImage( vk_info.dev, vk_info.swapchain.images[i], NULL );
    }
    vkDestroySwapchainKHR( vk_info.dev, vk_info.swapchain.swapchain, NULL );
    vkFreeCommandBuffers( vk_info.dev, vk_info.command_pool, 1, &vk_info.command_buffer );
    vkDestroyCommandPool( vk_info.dev, vk_info.command_pool, NULL );
    vkDestroyDevice( vk_info.dev, NULL );
    free( vk_info.queue_families );
    free( vk_info.gpus );
    DestroyDebugReporter( vk_info.inst );
    vkDestroyInstance( vk_info.inst, NULL );
}

void InitializeRenderingData( )
{
    float aspect = 16.0 / 9.0;
    state.player.model = GetDiagonalMatrix( 1.0f );
    state.rendering.projection = GetProjection( 120.0f, aspect, 0.1f, 100.0f );
    state.rendering.view = LookAt( (Vector3) { -5.0f, 3.0f, -10.0f },
                                   (Vector3) { 0.0f, 0.0f, 0.0f },
                                   (Vector3) { 0.0f, -1.0f, 0.0f } );
    state.rendering.clip.vec[0] = (Vector4) { 1.0f, 0.0f, 0.0f, 0.0f };
    state.rendering.clip.vec[1] = (Vector4) { 0.0f, -1.0f, 0.0f, 0.0f };
    state.rendering.clip.vec[2] = (Vector4) { 0.0f, 0.0f, 0.5f, 0.5f };
    state.rendering.clip.vec[3] = (Vector4) { 0.0f, 0.0f, 0.0f, 1.0f };

    LoadTexture( vk_info.gpus[0],
                 vk_info.dev,
                 vk_info.command_buffer,
                 vk_info.graphics_queue,
                 "img\\ice.bmp",
                 VK_FORMAT_R8G8B8A8_UNORM,
                 NULL,
                 &state.rendering.ice_texture );

    LoadTexture( vk_info.gpus[0],
                 vk_info.dev,
                 vk_info.command_buffer,
                 vk_info.graphics_queue,
                 "img\\map.bmp",
                 VK_FORMAT_R8G8B8A8_UNORM,
                 NULL,
                 &state.rendering.map_texture );

    LoadTexture( vk_info.gpus[0],
                 vk_info.dev,
                 vk_info.command_buffer,
                 vk_info.graphics_queue,
                 "img\\enemy.bmp",
                 VK_FORMAT_R8G8B8A8_UNORM,
                 NULL,
                 &state.rendering.enemy_texture );

    LoadTexture( vk_info.gpus[0],
                 vk_info.dev,
                 vk_info.command_buffer,
                 vk_info.graphics_queue,
                 "img\\sling.bmp",
                 VK_FORMAT_R8G8B8A8_UNORM,
                 NULL,
                 &state.rendering.sling_texture );

    LoadTexture( vk_info.gpus[0],
                 vk_info.dev,
                 vk_info.command_buffer,
                 vk_info.graphics_queue,
                 "img\\fireball.bmp",
                 VK_FORMAT_R8G8B8A8_UNORM,
                 NULL,
                 &state.rendering.fireball_texture );

    RGB colorkey;
    colorkey.r = 0;
    colorkey.g = 40;
    colorkey.b = 70;
    LoadFont( vk_info.gpus[0],
              vk_info.dev,
              vk_info.command_buffer,
              vk_info.graphics_queue,
              "img\\540x20Font.bmp",
              VK_FORMAT_R8G8B8A8_UNORM,
              &colorkey,
              "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
              &state.rendering.font );
    
    CreateDefaultSampler( vk_info.dev,
                          &state.rendering.texture_sampler );
    
    CreateVertexBuffer( vk_info.gpus[0],
                        vk_info.dev,
                        texture_cube,
                        SIZE_ARRAY( texture_cube ),
                        &state.rendering.cube_vertices );

    CreateVertexBuffer( vk_info.gpus[0],
                        vk_info.dev,
                        texture_square,
                        SIZE_ARRAY( texture_square ),
                        &state.rendering.square_vertices );

    CreateVertexBuffer( vk_info.gpus[0],
                        vk_info.dev,
                        texture_sling,
                        SIZE_ARRAY( texture_sling ),
                        &state.rendering.sling_vertices );

    CreateVertexBuffer( vk_info.gpus[0],
                        vk_info.dev,
                        texture_fireball,
                        SIZE_ARRAY( texture_fireball ),
                        &state.rendering.fireball_vertices );

    CreateUniformBuffer( vk_info.gpus[0],
                         vk_info.dev,
                         sizeof( Matrix4 ),
                         MAX_MODEL_COUNT,
                         &state.rendering.model_buffer );

    UpdateUniformDescriptorSet( vk_info.dev,
                                vk_info.descriptor_set,
                                &state.rendering.model_buffer,
                                0 );

    CreateUniformBuffer( vk_info.gpus[0],
                         vk_info.dev,
                         sizeof( Matrix4 ),
                         1,
                         &state.rendering.view_buffer );

    UpdateUniformDescriptorSet( vk_info.dev,
                                vk_info.descriptor_set,
                                &state.rendering.view_buffer,
                                1 );

}

void DestroyRenderingData()
{
    vkDestroySampler( vk_info.dev, state.rendering.texture_sampler, NULL );
    FreeImage( vk_info.dev, &state.rendering.ice_texture );
    FreeImage( vk_info.dev, &state.rendering.map_texture );
    FreeImage( vk_info.dev, &state.rendering.enemy_texture );
    FreeImage( vk_info.dev, &state.rendering.sling_texture );
    FreeImage( vk_info.dev, &state.rendering.fireball_texture );
    FreeVulkanBuffer( vk_info.dev, &state.rendering.cube_vertices.data );
    FreeVulkanBuffer( vk_info.dev, &state.rendering.square_vertices.data );
    FreeVulkanBuffer( vk_info.dev, &state.rendering.sling_vertices.data );
    FreeVulkanBuffer( vk_info.dev, &state.rendering.fireball_vertices.data );
    FreeVulkanBuffer( vk_info.dev, &state.rendering.model_buffer.data );
    FreeVulkanBuffer( vk_info.dev, &state.rendering.view_buffer.data );
    FreeFont( vk_info.dev, &state.rendering.font );
}
