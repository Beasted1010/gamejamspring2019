

#include <math.h>
#include "matrix.h"

 Vector3 VectAdd( Vector3 a, Vector3 b )
{
    Vector3 result;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    result.z = a.z + b.z;
    
    return result;
}

 Vector3 VectSub( Vector3 a, Vector3 b )
{
    Vector3 result;
    result.x = a.x - b.x;
    result.y = a.y - b.y;
    result.z = a.z - b.z;

    return result;
}

 Vector3 VectScale( Vector3 a, float scale )
{
    Vector3 result;

    result.x = scale * a.x;
    result.y = scale * a.y;
    result.z = scale * a.z;

    return result;
}

 float VectDot( Vector3 a, Vector3 b )
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

 float VectLengthSq( Vector3 vect )
{
    return VectDot( vect, vect );
}

 float VectLength( Vector3 vect )
{
    return sqrtf( VectLengthSq( vect ) );
}

 Vector3 VectNormalize( Vector3 vect )
{
    return VectScale( vect, 1 / VectLength( vect ) );
}

 Vector3 VectCross( Vector3 a, Vector3 b )
{
    Vector3 result;
    result.x = a.y * b.z - a.z * b.y;
    result.y = a.z * b.x - a.x * b.z;
    result.z = a.x * b.y - a.y * b.x;

    return result;
}

 Matrix4 Mult4( Matrix4 a, Matrix4 b )
{
    Matrix4 result;
    result.vec[0].x = a.vec[0].x * b.vec[0].x + a.vec[1].x * b.vec[0].y + a.vec[2].x * b.vec[0].z + a.vec[3].x * b.vec[0].w;
    result.vec[0].y = a.vec[0].y * b.vec[0].x + a.vec[1].y * b.vec[0].y + a.vec[2].y * b.vec[0].z + a.vec[3].y * b.vec[0].w;
    result.vec[0].z = a.vec[0].z * b.vec[0].x + a.vec[1].z * b.vec[0].y + a.vec[2].z * b.vec[0].z + a.vec[3].z * b.vec[0].w;
    result.vec[0].w = a.vec[0].w * b.vec[0].x + a.vec[1].w * b.vec[0].y + a.vec[2].w * b.vec[0].z + a.vec[3].w * b.vec[0].w;

    result.vec[1].x = a.vec[0].x * b.vec[1].x + a.vec[1].x * b.vec[1].y + a.vec[2].x * b.vec[1].z + a.vec[3].x * b.vec[1].w;
    result.vec[1].y = a.vec[0].y * b.vec[1].x + a.vec[1].y * b.vec[1].y + a.vec[2].y * b.vec[1].z + a.vec[3].y * b.vec[1].w;
    result.vec[1].z = a.vec[0].z * b.vec[1].x + a.vec[1].z * b.vec[1].y + a.vec[2].z * b.vec[1].z + a.vec[3].z * b.vec[1].w;
    result.vec[1].w = a.vec[0].w * b.vec[1].x + a.vec[1].w * b.vec[1].y + a.vec[2].w * b.vec[1].z + a.vec[3].w * b.vec[1].w;

    result.vec[2].x = a.vec[0].x * b.vec[2].x + a.vec[1].x * b.vec[2].y + a.vec[2].x * b.vec[2].z + a.vec[3].x * b.vec[2].w;
    result.vec[2].y = a.vec[0].y * b.vec[2].x + a.vec[1].y * b.vec[2].y + a.vec[2].y * b.vec[2].z + a.vec[3].y * b.vec[2].w;
    result.vec[2].z = a.vec[0].z * b.vec[2].x + a.vec[1].z * b.vec[2].y + a.vec[2].z * b.vec[2].z + a.vec[3].z * b.vec[2].w;
    result.vec[2].w = a.vec[0].w * b.vec[2].x + a.vec[1].w * b.vec[2].y + a.vec[2].w * b.vec[2].z + a.vec[3].w * b.vec[2].w;

    result.vec[3].x = a.vec[0].x * b.vec[3].x + a.vec[1].x * b.vec[3].y + a.vec[2].x * b.vec[3].z + a.vec[3].x * b.vec[3].w;
    result.vec[3].y = a.vec[0].y * b.vec[3].x + a.vec[1].y * b.vec[3].y + a.vec[2].y * b.vec[3].z + a.vec[3].y * b.vec[3].w;
    result.vec[3].z = a.vec[0].z * b.vec[3].x + a.vec[1].z * b.vec[3].y + a.vec[2].z * b.vec[3].z + a.vec[3].z * b.vec[3].w;
    result.vec[3].w = a.vec[0].w * b.vec[3].x + a.vec[1].w * b.vec[3].y + a.vec[2].w * b.vec[3].z + a.vec[3].w * b.vec[3].w;

    return result;
}

 Matrix4 GetDiagonalMatrix( float scalar )
{
    Matrix4 result;
    result.vec[0] = (Vector4) { scalar, 0.0f, 0.0f, 0.0f };
    result.vec[1] = (Vector4) { 0.0f, scalar, 0.0f, 0.0f };
    result.vec[2] = (Vector4) { 0.0f, 0.0f, scalar, 0.0f };
    result.vec[3] = (Vector4) { 0.0f, 0.0f, 0.0f, scalar };

    return result;
}

 Matrix4 GetProjection( float fov, float aspect, float zNear, float zFar )
{
    float tanhalffov = tanf( 0.5f * fov );

    Matrix4 result = { 0 };
    result.vec[0].x = 1 / (aspect * tanhalffov);
    result.vec[1].y = 1 / tanhalffov;
    result.vec[2].z = -(zFar + zNear) / (zFar - zNear);
    result.vec[2].w = -1.0f;
    result.vec[3].z = -(2.0f * zFar * zNear) / (zFar - zNear);

    return result;
}

 Matrix4 GetProjection2D( float left, float right, float bottom, float top,
                                float near_z, float far_z )
{
    float r_width  = 1.0f / (right - left);
    float r_height = 1.0f / (top - bottom);
    float r_depth  = 1.0f / (far_z - near_z);
    float x =  2.0f * (r_width);
    float y =  2.0f * (r_height);
    float z =  2.0f * (r_depth);
    float A = (right + left) * r_width;
    float B = (top + bottom) * r_height;
    float C = (far_z + near_z) * r_depth;

    Matrix4 result;
    result.vec[0].x = x;
    result.vec[1].x = 0.0f;
    result.vec[2].x = 0.0f;
    result.vec[3].x = -A;
    result.vec[0].y = 0.0f;
    result.vec[1].y = y;
    result.vec[2].y = 0.0f;
    result.vec[3].y = -B;
    result.vec[0].z = 0.0f;
    result.vec[1].z = 0.0f;
    result.vec[2].z = -z;
    result.vec[3].z = -C;
    result.vec[0].w = 0.0f;
    result.vec[1].w = 0.0f;
    result.vec[2].w = 0.0f;
    result.vec[3].w = 1.0f;

    return result;
}

 Matrix4 LookAt( Vector3 eye,
                       Vector3 center,
                       Vector3 up )
{
    Matrix4 result = GetDiagonalMatrix( 1.0f );

    Vector3 f = VectNormalize( VectSub( center, eye ) );
    Vector3 s = VectNormalize( VectCross( f, up ) );
    Vector3 u = VectCross( s, f );

    result.vec[0].x = s.x;
    result.vec[1].x = s.y;
    result.vec[2].x = s.z;

    result.vec[0].y = u.x;
    result.vec[1].y = u.y;
    result.vec[2].y = u.z;

    result.vec[0].z = -f.x;
    result.vec[1].z = -f.y;
    result.vec[2].z = -f.z;

    result.vec[3].x = -VectDot( s, eye );
    result.vec[3].y = -VectDot( u, eye );
    result.vec[3].z = VectDot( f, eye );

    return result;
}

Matrix4 GetScale( float scalex, float scaley, float scalez )
{
    Matrix4 scale = { 0 };
    scale.vec[0].x = scalex;
    scale.vec[1].y = scaley;
    scale.vec[2].z = scalez;
    scale.vec[3].w = 1;

    return scale;
}

 Matrix4 GetTranslation( float dx, float dy, float dz )
{
  /* 1 0 0 0 */
  /* 0 1 0 0  */
  /* 0 0 1 0 */
  /* x y z 1  */
    Matrix4 translate = GetDiagonalMatrix( 1.0f );

    translate.vec[3].x = dx;
    translate.vec[3].y = dy;
    translate.vec[3].z = dz;

    return translate;
}

 Matrix4 CopyTranslation( Matrix4 other )
{
    return GetTranslation( other.vec[3].x, other.vec[3].y, other.vec[3].z );
}

 Matrix4 GetRotation( float x_theta, float y_theta, float z_theta )
{
    float cosx = cosf( x_theta );
    float cosy = cosf( y_theta );
    float cosz = cosf( z_theta );

    float sinx = sinf( x_theta );
    float siny = sinf( y_theta );
    float sinz = sinf( z_theta );

    Matrix4 Rx;
    Rx.vec[0] = (Vector4) { 1.0f, 0.0f, 0.0f, 0.0f };
    Rx.vec[1] = (Vector4) { 0.0f, cosx, sinx, 0.0f };
    Rx.vec[2] = (Vector4) { 0.0f, -sinx, cosx, 0.0f };
    Rx.vec[3] = (Vector4) { 0.0f, 0.0f, 0.0f, 1.0f };

    Matrix4 Ry;
    Ry.vec[0] = (Vector4) { cosy, 0.0f, -siny, 0.0f };
    Ry.vec[1] = (Vector4) { 0.0f, 1.0f, 0.0f, 0.0f };
    Ry.vec[2] = (Vector4) { siny, 0.0f, cosy, 0.0f };
    Ry.vec[3] = Rx.vec[3];

    Matrix4 Rz;
    Rz.vec[0] = (Vector4) { cosz, sinz, 0.0f, 0.0f };
    Rz.vec[1] = (Vector4) { -sinz, cosz, 0.0f, 0.0f };
    Rz.vec[2] = (Vector4) { 0.0f, 0.0f, 1.0f, 0.0f };
    Rz.vec[3] = Ry.vec[3];

    return Mult4( Rz, Mult4( Ry, Rx ) );
}
