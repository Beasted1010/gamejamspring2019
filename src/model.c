#include "common.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "matrix.h"
#include "vulkan_wrapper.h"
#include "model.h"

ColorVertex* ExtrudeModel( const ColorVertex* front_face, uint32_t face_count,
                           const ColorVertex* front_edge, uint32_t edge_count,
                           float relative_z, uint32_t* result_count )
{
    uint32_t total_vert = face_count * 2 + edge_count * 6;

    ColorVertex* result = malloc( total_vert * sizeof( ColorVertex ) );

    if ( !result )
    {
        return NULL;
    }

    uint32_t front_face_index;
    uint32_t back_face_index;
    if ( relative_z < 0 )
    {
        front_face_index = 0;
        back_face_index = face_count;
    }
    else
    {
        front_face_index = face_count;
        back_face_index = 0;
    }

    memcpy( result + front_face_index, front_face, face_count * sizeof( ColorVertex ) );

    uint32_t i;
    for ( i = 0; i < face_count; i++ )
    {
        if ( i % 3 == 0 )
        {
            result[back_face_index + i] = front_face[i];
        }
        else if ( i % 3 == 1 )
        {
            result[back_face_index + i] = front_face[i + 1];
        }
        else if ( i % 3 == 2 )
        {
            result[back_face_index + i] = front_face[i - 1];
        }

        result[face_count + i].z = result[face_count + i].z + relative_z;
    }

    for ( i = 0; i < edge_count; i++ )
    {
        uint32_t base_index = 2 * face_count + 6 * i;
        
        if ( relative_z < 0 )
        {
            result[base_index] = front_edge[i];
            result[base_index + 1] = front_edge[i];
            result[base_index + 1].z = result[base_index + 1].z + relative_z;
            result[base_index + 2] = front_edge[(i + 1) % edge_count];

            result[base_index + 3] = result[base_index + 1];
            result[base_index + 4] = result[base_index + 2];
            result[base_index + 4].z = result[base_index + 4].z + relative_z;
            result[base_index + 5] = result[base_index + 2];
        }
        else 
        {
            result[base_index] = front_edge[i];
            result[base_index].z = result[base_index].z + relative_z;
            result[base_index + 1] = front_edge[i];
            result[base_index + 2] = front_edge[(i + 1) % edge_count];
            result[base_index + 2].z = result[base_index + 2].z + relative_z;

            result[base_index + 3] = result[base_index + 1];
            result[base_index + 4] = result[base_index + 2];
            result[base_index + 4].z = result[base_index + 4].z - relative_z;
            result[base_index + 5] = result[base_index + 2];
        }
    }

    if ( result_count )
    {
        *result_count = total_vert;
    }
    
    return result;
}

void CalculateNormals( const TextureVertex* vertices, uint32_t triangle_count, Vector3* normals )
{
    uint32_t i;
    for ( i = 0; i < triangle_count; i++ )
    {
        Vector3 first;
        first.x = vertices[3 * i + 2].x - vertices[3 * i].x;
        first.y = vertices[3 * i + 2].y - vertices[3 * i].y;
        first.z = vertices[3 * i + 2].z - vertices[3 * i].z;

        Vector3 second;
        second.x = vertices[3 * i + 1].x - vertices[3 * i].x;
        second.y = vertices[3 * i + 1].y - vertices[3 * i].y;
        second.z = vertices[3 * i + 1].z - vertices[3 * i].z;

        normals[i] = VectNormalize( VectCross( first, second ) );
    }
}

void ScaleModel( ColorVertex* vertices, uint32_t count, float scale )
{
    uint32_t i;
    for ( i = 0; i < count; i++ )
    {
        vertices[i].x *= scale;
        vertices[i].y *= scale;
        vertices[i].z *= scale;
    }
}

void StretchModel( ColorVertex* vertices, uint32_t count, Vector3 stretch )
{
    uint32_t i;
    for ( i = 0; i < count; i++ )
    {
        vertices[i].x *= stretch.x;
        vertices[i].y *= stretch.y;
        vertices[i].z *= stretch.z;
    }
}

void TranslateModel( ColorVertex* vertices, uint32_t count, Vector3 offset )
{
    uint32_t i;
    for ( i = 0; i < count; i++ )
    {
        vertices[i].x = vertices[i].x + offset.x;
        vertices[i].y = vertices[i].y + offset.y;
        vertices[i].z = vertices[i].z + offset.z;
    }
}

void PrintModel( const ColorVertex* vertices, uint32_t count )
{
    printf( "#ifndef MODEL_%u_H\n", count );
    printf( "#define MODEL_%u_H\n", count );

    printf( "#include \"vulkan_wrapper.h\"\n" );
    printf( "#define XYZ1(_x_, _y_, _z_) (_x_), (_y_), (_z_), 1.0f\n" );

    printf( "ColorVertex data[] = {\n" );

    uint32_t i;
    for ( i = 0; i < count; i++ )
    {
        printf( "    { XYZ1( %0.7f, %0.7f, %0.7f ), XYZ1( %0.7f, %0.7f, %0.7f ) },\n",
                vertices[i].x,
                vertices[i].y,
                vertices[i].z,
                vertices[i].r,
                vertices[i].g,
                vertices[i].b );

        if ( i % 3 == 2 )
        {
            printf( "\n" );
        }
    }

    printf( "};\n" );

    printf( "#endif\n" );
}
