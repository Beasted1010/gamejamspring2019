#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define LOG_LEVEL LOG_QUIET
#include "vulkan_wrapper.h"
#include "matrix.h"
#include "model.h"

int WriteToVulkanBuffer( const VkDevice dev,
                         VulkanBuffer* buffer, 
                         const void* write_data, 
                         size_t size )
{
    void* data_copy_loc;
    VkResult check = vkMapMemory( dev, buffer->mem, 0, size, 0, &data_copy_loc );
    if ( check )
    {
        init_log( "Failed to map buffer memory to CPU.\n" );
        return 1;
    }
    
    if ( write_data )
    {
        memcpy( data_copy_loc, write_data, size );
    }
    else
    {
        memset( data_copy_loc, 0, size );
    }

    vkUnmapMemory( dev, buffer->mem );

    return 0;
}

int CreateVulkanBuffer( const VkPhysicalDevice phys_dev,
                        const VkDevice dev,
                        VulkanBuffer* buffer, 
                        uint32_t usage, 
                        const void* write_data, 
                        size_t size )
{
    VkBufferCreateInfo buffer_create = { 0 };
    buffer_create.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_create.pNext = NULL;
    buffer_create.usage = usage;
    buffer_create.size = size;
    buffer_create.queueFamilyIndexCount = 0;
    buffer_create.pQueueFamilyIndices = NULL;
    buffer_create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    buffer_create.flags = 0;

    VkResult check = vkCreateBuffer( dev, &buffer_create, NULL, &buffer->buffer );
    if ( check )
    {
        init_log( "Failed to create data buffer.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created data buffer.\n" );
    }

    VkMemoryRequirements memory_requirements;
    vkGetBufferMemoryRequirements( dev, buffer->buffer, &memory_requirements );

    VkPhysicalDeviceMemoryProperties memory_properties;
    vkGetPhysicalDeviceMemoryProperties( phys_dev, &memory_properties );

    VkMemoryAllocateInfo buffer_alloc = { 0 };
    buffer_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    buffer_alloc.pNext = NULL;
    buffer_alloc.memoryTypeIndex = 0;
    buffer_alloc.allocationSize = memory_requirements.size;

    check = GetMemoryTypeFromDeviceProperties( &memory_properties, 
                                               memory_requirements.memoryTypeBits,
                                               VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                               VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                               &buffer_alloc.memoryTypeIndex );

    if ( check )
    {
        init_log( "Failed to get proper memory type for buffer.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully found memory type for buffer.\n" );
    }

    check = vkAllocateMemory( dev, &buffer_alloc, NULL, &buffer->mem );

    if ( check )
    {
        init_log( "Failed to allocate buffer memory.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully allocated buffer memory.\n" );
    }

    WriteToVulkanBuffer( dev, buffer, write_data, size );

    check = vkBindBufferMemory( dev, buffer->buffer, buffer->mem, 0 );
    if ( check )
    {
        init_log( "Failed to bind data memory to the buffer.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully bound data memory to the buffer.\n" );
    }

    buffer->descriptor_info.buffer = buffer->buffer;
    buffer->descriptor_info.offset = 0;
    buffer->descriptor_info.range = size;

    return 0;
}

void FreeVulkanBuffer( const VkDevice dev,
                       VulkanBuffer* buffer )
{
    vkFreeMemory( dev, buffer->mem, NULL );
    vkDestroyBuffer( dev, buffer->buffer, NULL );
}

int CreateUniformBuffer( const VkPhysicalDevice phys_dev, 
                         const VkDevice dev,
                         size_t element_size,
                         size_t num,
                         UniformBuffer* buf )    
{
    if ( num > 1 )
    {
        VkPhysicalDeviceProperties phys_properties;
        buf->is_dynamic = 1;
	    vkGetPhysicalDeviceProperties( phys_dev, &phys_properties );
	    if (phys_properties.limits.minUniformBufferOffsetAlignment > element_size )
    	{
		    buf->alignment = phys_properties.limits.minUniformBufferOffsetAlignment;
	    }
	    else
	    {
            buf->alignment = element_size;
	    }

        return CreateVulkanBuffer( phys_dev, 
                                   dev, 
                                   &buf->data, 
                                   VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                   0,
                                   buf->alignment * num );
    }
    else
    {
        buf->is_dynamic = 0;
        return CreateVulkanBuffer( phys_dev,
                                   dev,
                                   &buf->data,
                                   VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                   0, 
                                   element_size );
    }
}

int WriteToUniformBuffer( const VkDevice dev,
                          UniformBuffer* buf,
                          size_t element_size,
                          size_t num,
                          const void* write_data )
{
    void* data_copy_loc;
    VkResult check = vkMapMemory( dev, buf->data.mem, 0, buf->alignment * num, 0, &data_copy_loc );
    if ( check )
    {
        init_log( "Failed to map buffer memory to CPU.\n" );
        return 1;
    }
    
    if ( write_data )
    {
        uint32_t i;
        for ( i = 0; i < num; i++ )
        {
            memcpy( data_copy_loc + buf->alignment * i, write_data + element_size * i, element_size );
        }
    }
    else
    {
        memset( data_copy_loc, 0, buf->alignment * num );
    }

    vkUnmapMemory( dev, buf->data.mem );

    return 0;
}

int ReallocUniformBuffer( const VkPhysicalDevice phys_dev,
                          const VkDevice dev,
                          size_t element_size,
                          size_t num,
                          UniformBuffer* buf )
{
    // TODO > This is a dumb and expensive way to do this.
    FreeVulkanBuffer( dev, &buf->data );
    return CreateUniformBuffer( phys_dev, dev, element_size, num, buf );
}

typedef struct
{
    float x, y, z, w, u, v, xn, yn, zn, wn;
} NormalVertex;

int CreateVertexBuffer( const VkPhysicalDevice phys_dev,
                        const VkDevice dev, 
                        const TextureVertex* vertices, 
                        uint32_t vertex_count,
                        VertexBuffer* buf )
{
    uint32_t triangle_count = vertex_count / 3;
    
    Vector3* normals = malloc( sizeof( Vector3 ) * triangle_count );
    CalculateNormals( vertices, triangle_count, normals );
    NormalVertex* full_vertices = malloc( sizeof( NormalVertex ) * vertex_count );

    uint32_t i;
    for ( i = 0; i < vertex_count; i++ )
    {
        full_vertices[i].x = vertices[i].x;
        full_vertices[i].y = vertices[i].y;
        full_vertices[i].z = vertices[i].z;
        full_vertices[i].w = vertices[i].w;
        full_vertices[i].u = vertices[i].u;
        full_vertices[i].v = vertices[i].v;
        full_vertices[i].xn = normals[i / 3].x;
        full_vertices[i].yn = normals[i / 3].y;
        full_vertices[i].zn = normals[i / 3].z;
        full_vertices[i].wn = 0.0f;
    }

    int result = CreateVulkanBuffer( phys_dev,
                                     dev,
                                     &buf->data,
                                     VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                                     full_vertices,
                                     sizeof( *full_vertices ) * vertex_count );

    buf->vertex_count = vertex_count;

    free( full_vertices );
    free( normals );

    return result;
}
