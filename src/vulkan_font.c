
#include "string.h"

#include "platform.h"
#include "common.h"
#include "matrix.h"
#include "model.h"
#include "vulkan_wrapper.h"

#define MAX_STRING_LENGTH 256

void LoadFont( const VkPhysicalDevice phys_dev,
               const VkDevice dev,
               const VkCommandBuffer command_buffer,
               const VkQueue graphics_queue,
               const char* filename,
               uint32_t format,
               const RGB* colorkey,
               const char* character_map,
               VulkanFont* font )
{
    LoadTexture( phys_dev,
                 dev,
                 command_buffer,
                 graphics_queue,
                 filename,
                 format,
                 colorkey,
                 &font->image_data );

    font->character_map = character_map;
    int len = strlen( character_map );
    int per_char_width = font->image_data.width / len;
    font->char_width = per_char_width;
    
    float normalized_width = 1.0f / len;
    float triangle_width = per_char_width / (float) font->image_data.height;
    float x_pt = triangle_width / 2;

    TextureVertex* raw_vertices = malloc( sizeof( TextureVertex ) * len * 6 );
    int i;
    for ( i = 0; i < len; i++ )
    {
        // Upper left-hand triangle
        raw_vertices[6 * i + 0] =
            (TextureVertex){ XYZ1( -x_pt, 0, 0 ), UV( i * normalized_width, 1 ) };
        raw_vertices[6 * i + 1] =
            (TextureVertex){ XYZ1( -x_pt, 0, 1 ), UV( i * normalized_width, 0 ) };
        raw_vertices[6 * i + 2] =
            (TextureVertex){ XYZ1( x_pt, 0, 0 ), UV( (i + 1) * normalized_width, 1 ) };

        // Lower right-hand triangle
        raw_vertices[6 * i + 3] =
            (TextureVertex){ XYZ1( -x_pt, 0, 1 ), UV( i * normalized_width, 0 ) };
        raw_vertices[6 * i + 4] =
            (TextureVertex){ XYZ1( x_pt, 0, 1 ), UV( (i + 1) * normalized_width, 0 ) };
        raw_vertices[6 * i + 5] =
            (TextureVertex){ XYZ1( x_pt, 0, 0 ), UV( (i + 1) * normalized_width, 1 ) };
        
    }

    CreateVertexBuffer( phys_dev, dev, raw_vertices, 6 * len, &font->vertices );
    free( raw_vertices );

    CreateUniformBuffer( phys_dev, dev, sizeof( Matrix4 ), MAX_STRING_LENGTH, &font->model_buffer );
}

void RenderText( const VkDevice dev,
                 const VkCommandBuffer command_buffer,
                 const VkPipelineLayout pipeline_layout,
                 const VkDescriptorSet descriptor_set,
                 VulkanFont* font,
                 const char* text,
                 float height,
                 Matrix4 *rotation,
                 Vector3 location,
                 Vector3 direction )
{
    int len = strlen( text );
    int maplen = strlen( font->character_map );
    
    Matrix4* models = malloc( sizeof( Matrix4 ) * len );
    float width = height * (font->char_width / (float)font->image_data.height );

    Vector3 inc = VectScale( VectNormalize( direction ), width );
    
    Matrix4 scale = GetScale( height, 0, height );

    int i;
    int xmult = 0, ymult = 0;
    for ( i = 0; i < len; i++, xmult++ )
    {
        if ( text[i] == '\n' )
        {
            xmult = -1;
            ymult++;
            continue;
        }
        else if ( text[i] == '\t' )
        {
            xmult = (xmult / 4) * 4 + 3;
            continue;
        }
        
        Matrix4 translation = GetTranslation( location.x + inc.x * xmult,
                                              location.y + inc.y * xmult,
                                              location.z + inc.z * xmult - (height + 0.2) * ymult );
        models[i] = Mult4( translation, Mult4( *rotation, scale ) );
    }

    WriteToUniformBuffer( dev, &font->model_buffer, sizeof( Matrix4 ), len, models );
    free( models );

    for ( i = 0; i < len; i++ )
    {
        char desired = text[i];
        if ( desired == ' ' )
        {
            continue;
        }
        
        int search;
        for ( search = 0; search < maplen; search++ )
        {
            if ( font->character_map[search] == desired )
            {
                break;
            }
        }

        if ( search == maplen )
        {
            continue;
        }
        
        VkDeviceSize offsets[1] = { 60 * sizeof( float ) * search };
        vkCmdBindVertexBuffers( command_buffer,
                                0,
                                1,
                                &font->vertices.data.buffer,
                                offsets );
                                
        uint32_t dynamic_offset = i * font->model_buffer.alignment;
        vkCmdBindDescriptorSets( command_buffer,
                                 VK_PIPELINE_BIND_POINT_GRAPHICS,
                                 pipeline_layout,
                                 0,
                                 1,
                                 &descriptor_set,
                                 1,
                                 &dynamic_offset );

        vkCmdDraw( command_buffer, 6, 1, 0, 0 );
    }
}
               
void FreeFont( const VkDevice dev, VulkanFont* font )
{
    FreeImage( dev, &font->image_data );
    FreeVulkanBuffer( dev, &font->model_buffer.data );
    FreeVulkanBuffer( dev, &font->vertices.data );
}
