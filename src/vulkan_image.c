#include "common.h"

#include "stdio.h"
#include "stdlib.h"

//#define LOG_LEVEL LOG_QUIET
#include "vulkan_wrapper.h"
#include "platform.h"

int CreateVulkanImage( const VkPhysicalDevice phys_dev,
                       const VkDevice dev,
                       uint32_t width,
                       uint32_t height,
                       uint32_t usage,
                       uint32_t tiling_mode,
                       uint32_t format,
                       uint32_t view_aspect,
                       VulkanImage* image )
{
    VkImageCreateInfo image_create;

    image->format = format;
    image->layout = VK_IMAGE_LAYOUT_UNDEFINED;
    image->width = width;
    image->height = height;
    image_create.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_create.pNext = NULL;
    image_create.imageType = VK_IMAGE_TYPE_2D;
    image_create.extent.width = width;
    image_create.extent.height = height;
    image_create.extent.depth = 1;
    image_create.mipLevels = 1;
    image_create.arrayLayers = 1;
    image_create.samples = 1;
    image_create.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    image_create.format = format;
    image_create.tiling = tiling_mode;
    image_create.usage = usage;
    image_create.queueFamilyIndexCount = 0;
    image_create.pQueueFamilyIndices = NULL;
    image_create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_create.flags = 0;

    VkResult check = vkCreateImage( dev, &image_create, NULL, &image->image );
    if ( check )
    {
        init_log( "Failed to create Depth Buffer image.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created depth buffer image.\n" );
    }

    VkPhysicalDeviceMemoryProperties memory_properties;
    vkGetPhysicalDeviceMemoryProperties( phys_dev, &memory_properties );

    VkMemoryRequirements memory_requirements;
    vkGetImageMemoryRequirements( dev, image->image, &memory_requirements );

    VkMemoryAllocateInfo mem_alloc = { 0 };
    mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mem_alloc.pNext = NULL;
    mem_alloc.allocationSize = memory_requirements.size;
    check = GetMemoryTypeFromDeviceProperties( &memory_properties, 
                                               memory_requirements.memoryTypeBits,
                                               0, 
                                               &mem_alloc.memoryTypeIndex );
    if ( check )
    {
        init_log( "Failed to get Memory Type from physical device properties.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully found memory type with index %u from device properties.\n",
            mem_alloc.memoryTypeIndex );
    }

    check = vkAllocateMemory( dev, &mem_alloc, NULL, &image->mem );
    if ( check )
    {
        init_log( "Failed to allocate depth buffer memory.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully allocated depth buffer memory.\n" );
    }

    check = vkBindImageMemory( dev, image->image, image->mem, 0 );
    if ( check )
    {
        init_log( "Failed to bind depth buffer image memory.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully bound image memory.\n" );
    }

    VkImageViewCreateInfo image_view_create = { 0 };
    image_view_create.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_create.pNext = NULL;
    image_view_create.image = image->image;
    image_view_create.format = format;
    image_view_create.components.r = VK_COMPONENT_SWIZZLE_R;
    image_view_create.components.g = VK_COMPONENT_SWIZZLE_G;
    image_view_create.components.b = VK_COMPONENT_SWIZZLE_B;
    image_view_create.components.a = VK_COMPONENT_SWIZZLE_A;
    image_view_create.subresourceRange.aspectMask = view_aspect;
    image_view_create.subresourceRange.baseMipLevel = 0;
    image_view_create.subresourceRange.levelCount = 1;
    image_view_create.subresourceRange.baseArrayLayer = 0;
    image_view_create.subresourceRange.layerCount = 1;
    image_view_create.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_create.flags = 0;

    check = vkCreateImageView( dev, &image_view_create, NULL, &image->view );
    if ( check )
    {
        init_log( "Failed to create image view for depth buffer image.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created image view for depth buffer.\n" );
    }

    return 0;
}

int CreateTextureImage( const VkPhysicalDevice phys_dev,
                        const VkDevice dev,
                        uint32_t width,
                        uint32_t height,
                        uint32_t format,
                        VulkanImage* tex_image )
{
    return CreateVulkanImage( phys_dev,
                              dev,
                              width,
                              height,
                              VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                              VK_IMAGE_TILING_OPTIMAL,
                              format,
                              VK_IMAGE_ASPECT_COLOR_BIT,
                              tex_image );
}

int LoadTexture( const VkPhysicalDevice phys_dev,
                 const VkDevice dev,
                 const VkCommandBuffer command_buffer,
                 const VkQueue graphics_queue,
                 const char* filename,
                 uint32_t format,
                 const RGB* colorkey,
                 VulkanImage* tex_image )
{
    Bitmap* bmp = LoadBitmapFromFile( filename, colorkey );
    if ( !bmp )
    {
        return 1;
    }

    uint32_t w = GetBitmapWidth( bmp );
    uint32_t h = GetBitmapHeight( bmp );
    size_t img_size = 4 * w * h;
    void* pixels = GetBitmapPixels( bmp );

    VulkanBuffer staging_buffer;
    if ( CreateVulkanBuffer( phys_dev,
                             dev,
                             &staging_buffer,
                             VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                             pixels,
                             img_size ) )
    {
        return 1;
    }

    FreeBitmap( bmp );

    if ( CreateTextureImage( phys_dev,
                             dev,
                             w,
                             h,
                             format,
                             tex_image ) )
    {
        return 1;
    }

    TransitionImageLayout( command_buffer,
                           graphics_queue,
                           tex_image,
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL );

    CopyBufferToImage( command_buffer,
                       graphics_queue,
                       &staging_buffer,
                       tex_image );

    TransitionImageLayout( command_buffer,
                           graphics_queue,
                           tex_image,
                           VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL );

    FreeVulkanBuffer( dev, &staging_buffer );

    return 0;    
}

// Creates the depth buffer image, allocates memory for it, and creates a view.
int CreateDepthImage( const VkPhysicalDevice phys_dev,
                      const VkDevice dev,
                      uint32_t width,
                      uint32_t height,
                      VulkanImage* depth_image )
{
    uint32_t tiling_mode;
    VkFormatProperties depth_format_properties;
    vkGetPhysicalDeviceFormatProperties( phys_dev, VK_FORMAT_D16_UNORM, &depth_format_properties );
    if ( depth_format_properties.linearTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT )
    {
        init_log( "Chose linear tiling for depth buffer.\n" );
        tiling_mode = VK_IMAGE_TILING_LINEAR;
    }
    else if ( depth_format_properties.optimalTilingFeatures &
              VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT )
    {
        init_log( "Chose optimal tiling for depth buffer.\n" );
        tiling_mode = VK_IMAGE_TILING_OPTIMAL;
    }
    else
    {
        init_log( "Failed to use D16 UNORM format because it is unsupported.\n" );
        return 1;
    }

    return CreateVulkanImage( phys_dev,
                              dev,
                              width,
                              height,
                              VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                              tiling_mode,
                              VK_FORMAT_D16_UNORM,
                              VK_IMAGE_ASPECT_DEPTH_BIT,
                              depth_image );
}

void FreeImage( const VkDevice dev, VulkanImage* image )
{
    vkDestroyImageView( dev, image->view, NULL );
    vkFreeMemory( dev, image->mem, NULL );
    vkDestroyImage( dev, image->image, NULL );
}

void CopyBufferToImage( const VkCommandBuffer command_buffer,
                        const VkQueue graphics_queue,
                        const VulkanBuffer* buffer,
                        const VulkanImage* image )
{
    BeginCommandBuffer( command_buffer );
    
    VkBufferImageCopy region = { 0 };
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageOffset = (VkOffset3D){ 0, 0, 0 };
    region.imageExtent = (VkExtent3D){ image->width, image->height, 1 };

    vkCmdCopyBufferToImage( command_buffer,
                            buffer->buffer,
                            image->image,
                            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                            1,
                            &region );

    EndCommandBuffer( command_buffer );
    SubmitBasicQueue( graphics_queue, command_buffer );
}

void TransitionImageLayout( const VkCommandBuffer command_buffer,
                            const VkQueue graphics_queue,
                            VulkanImage* image,
                            VkImageLayout new_layout )
{
    BeginCommandBuffer( command_buffer );

    VkPipelineStageFlags source_stage, destination_stage;

    VkImageMemoryBarrier barrier = { 0 };
    if ( image->layout  == VK_IMAGE_LAYOUT_UNDEFINED &&
         new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL )
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if ( image->layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
              new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else
    {
        printf( "Error: unsupported transition.\n" );
        exit( 1 );
    }
    
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = image->layout;
    barrier.newLayout = new_layout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image->image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = 0;

    vkCmdPipelineBarrier(
        command_buffer,
        source_stage, destination_stage,
        0,
        0, NULL,
        0, NULL,
        1, &barrier );
    
    EndCommandBuffer( command_buffer );
    SubmitBasicQueue( graphics_queue, command_buffer );

    image->layout = new_layout;
}
