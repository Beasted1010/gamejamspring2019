#include "stdio.h"
#include "stdlib.h"

//#define LOG_LEVEL LOG_QUIET
#include "common.h"
#include "vulkan_wrapper.h"
#include "listmalloc.h"

typedef struct SurfaceQueryInfo
{
    VkPhysicalDevice gpu;
    VkSurfaceKHR surf;
} SurfaceQueryInfo;

typedef struct SwapchainQueryInfo
{
    VkDevice dev;
    VkSwapchainKHR swapchain;
} SwapchainQueryInfo;

typedef struct SurfaceFormatQueryInfo
{
    VkPhysicalDevice gpu;
    VkSurfaceKHR surf;
} SurfaceFormatQueryInfo;

VulkanListMallocFunction( VkPhysicalDevice, vkEnumeratePhysicalDevices, VkInstance, PhysicalDevices, "physical device", 1, a );
VulkanListMallocFunction( VkQueueFamilyProperties, vkGetPhysicalDeviceQueueFamilyProperties, VkPhysicalDevice, QueueFamilyProperties, "queue family", 0, a );
VulkanListMallocFunction( VkSurfaceFormatKHR, vkGetPhysicalDeviceSurfaceFormatsKHR, SurfaceQueryInfo, SurfaceFormats, "surface format", 1, a.gpu, a.surf );
VulkanListMallocFunction( VkPresentModeKHR, vkGetPhysicalDeviceSurfacePresentModesKHR, SurfaceQueryInfo, SurfacePresentModes, "present mode", 1, a.gpu, a.surf );
VulkanListMallocFunction( VkImage, vkGetSwapchainImagesKHR, SwapchainQueryInfo, SwapchainImages, "swapchain image", 1, a.dev, a.swapchain );

// Initializes the app information of the Vulkan instance and
// initializes the instance itself.
int InitializeInstance( VkApplicationInfo* app_info, 
                        VkInstance* inst, 
                        const char* app_name )
{
    app_info->sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info->pNext = NULL;
    app_info->pApplicationName = app_name;
    app_info->applicationVersion = 0;
    app_info->pEngineName = app_name;
    app_info->engineVersion = 1;
    app_info->apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo inst_info = { 0 };
    inst_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    inst_info.pNext = NULL;
    inst_info.flags = 0;
    inst_info.pApplicationInfo = app_info;
    inst_info.enabledExtensionCount = vulkan_instance_extension_num;
    inst_info.ppEnabledExtensionNames = vulkan_instance_extension_names;
    inst_info.enabledLayerCount = 0;
    inst_info.ppEnabledLayerNames = NULL;

    VkResult check;

    check = vkCreateInstance( &inst_info, NULL, inst );
    if ( check == VK_ERROR_INCOMPATIBLE_DRIVER )
    {
        init_log( "Failed to create a Vulkan Instance due to driver incompatibility.\n" );
        return 1;
    }
    else if ( check )
    {
        init_log( "Failed to create a Vulkan Instance.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created a Vulkan Instance.\n" );
    }

    return 0;
}

// Finds and initializes Physical Devices.
VkPhysicalDevice* GetPhysicalDevices( const VkInstance inst, uint32_t* count )
{
    return GetPhysicalDevicesMalloc( inst, count );
}

// This function sets up the queue families and discovers indices
// for a present queue and a graphics queue.
VkQueueFamilyProperties* GetQueueFamilies( const VkPhysicalDevice phys_dev,
                                           const VkSurfaceKHR surface,
                                           uint32_t* graphics_queue_index,
                                           uint32_t* present_queue_index,
                                           uint32_t* queue_count )
{

    // For now, assume that the first physical device is adequate
    VkQueueFamilyProperties* result = GetQueueFamilyPropertiesMalloc( phys_dev, queue_count );
    if ( !result )
    {
        init_log( "Failed to get queue family properties.\n" );
        return NULL;
    }

    {
        VkBool32* supports_present = malloc( sizeof( *supports_present ) * *queue_count );

        uint32_t i;
        for ( i = 0; i < *queue_count; i++ )
        {
            vkGetPhysicalDeviceSurfaceSupportKHR( phys_dev, i, surface, supports_present + i );
        }

        *graphics_queue_index = *queue_count;
        *present_queue_index = *queue_count;
        for ( i = 0; i < *queue_count; i++ )
        {
            if ( result[i].queueFlags & VK_QUEUE_GRAPHICS_BIT )
            {
                *graphics_queue_index = i;
            }
            
            if ( supports_present[i] )
            {
                *present_queue_index = i;
            }

            if ( *graphics_queue_index == *present_queue_index )
            {
                // If we can, use the same queue for both present and graphics
                break;
            }
        }

        free( supports_present );
    }

    if ( *graphics_queue_index == *queue_count )
    {
        init_log( "Failed to find graphics queue.\n" );
        free( result );
        return NULL;
    }
    else
    {
        init_log( "Successfully chose queue index %u for graphics.\n", *graphics_queue_index );
    }

    if ( *present_queue_index == *queue_count )
    {
        init_log( "Failed to find present queue.\n" );
        free( result );
        return NULL;
    }
    else
    {
        init_log( "Successfully chose queue index %u for presenting.\n", *present_queue_index );
    }

    return result;
}

// Initializes the logical device that will be used for all
// graphics operations. This function makes use of
// the previously initialized queue family.
int InitializeLogicalDevice( const VkPhysicalDevice phys_dev, 
                             uint32_t graphics_queue_index, 
                             VkDevice* dev )
{
    VkDeviceQueueCreateInfo queue_info = { 0 };
    queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queue_info.pNext = NULL;
    queue_info.flags = 0;
    queue_info.queueCount = 1;
    queue_info.queueFamilyIndex = graphics_queue_index;

    float queue_priorities[1] = { 0.0 };
    queue_info.pQueuePriorities = queue_priorities;

    VkDeviceCreateInfo dev_info = { 0 };
    dev_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    dev_info.pNext = NULL;
    dev_info.flags = 0;
    dev_info.queueCreateInfoCount = 1;
    dev_info.pQueueCreateInfos = &queue_info;
    
    // The following two values are deprecated and ignored
    dev_info.enabledLayerCount = 0;
    dev_info.ppEnabledLayerNames = NULL;

    const char* device_extensions[1] =
    {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    dev_info.enabledExtensionCount = SIZE_ARRAY( device_extensions );
    dev_info.ppEnabledExtensionNames = device_extensions;

    VkPhysicalDeviceFeatures required_features = { 0 };
    required_features.samplerAnisotropy = VK_TRUE;
    
    dev_info.pEnabledFeatures = &required_features;

    VkResult check = vkCreateDevice( phys_dev, &dev_info, NULL, dev );
    if ( check == VK_ERROR_TOO_MANY_OBJECTS )
    {
        init_log( "Failed: device could not be created due to lack of device-specific resources.\n" );
        return 1;
    }
    else if ( check )
    {
        init_log( "Failed to create logical device.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created logical device.\n" );
    }

    return 0;
}

// Creates and allocates the command pool/buffer.
int InitializeCommandPool( const VkDevice dev,
                           uint32_t graphics_queue_index, 
                           VkCommandPool* pool, 
                           VkCommandBuffer* buffers, 
                           uint32_t count )
{
    VkCommandPoolCreateInfo cmd_pool_info = { 0 };
    cmd_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmd_pool_info.pNext = NULL;
    cmd_pool_info.flags = 0;
    cmd_pool_info.queueFamilyIndex = graphics_queue_index;

    VkResult check = vkCreateCommandPool( dev, &cmd_pool_info, NULL, pool );
    if ( check )
    {
        init_log( "Failed to create a command pool.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created command pool.\n" );
    }

    VkCommandBufferAllocateInfo cmd_alloc_info = { 0 };
    cmd_alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmd_alloc_info.pNext = NULL;
    cmd_alloc_info.commandPool = *pool;
    cmd_alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cmd_alloc_info.commandBufferCount = count;

    check = vkAllocateCommandBuffers( dev, &cmd_alloc_info, buffers );
    if ( check )
    {
        init_log( "Failed to allocate command buffer.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully allocated command buffer.\n" );
    }

    return 0;
}

// Initializes the swapchain for rendering usage.
int InitializeSwapchain( const VkPhysicalDevice phys_dev,
                         const VkDevice dev,
                         const VkSurfaceKHR surface,
                         uint32_t display_width,
                         uint32_t display_height,
                         uint32_t graphics_queue_index,
                         uint32_t present_queue_index,
                         VulkanSwapchain* swapchain )
{
    VkSwapchainCreateInfoKHR swapchain_create_info;
    swapchain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchain_create_info.pNext = NULL;
    swapchain_create_info.surface = surface;

    SurfaceQueryInfo query_info;
    query_info.gpu = phys_dev;
    query_info.surf = surface;
    uint32_t format_count;
    VkSurfaceFormatKHR* formats = GetSurfaceFormatsMalloc( query_info, &format_count );
    if ( !formats )
    {
        init_log( "Failed to get surface formats.\n" );
        return 1;
    }

    VkFormat selected_format;
    if ( format_count >= 1 && formats[0].format != VK_FORMAT_UNDEFINED )
    {
        selected_format = formats[0].format;
    }
    else
    {
        selected_format = VK_FORMAT_B8G8R8A8_UNORM;
    }
    
    swapchain->format = selected_format;

    VkSurfaceCapabilitiesKHR surface_capabilities;
    VkResult check = vkGetPhysicalDeviceSurfaceCapabilitiesKHR( phys_dev, surface, &surface_capabilities );
    if ( check )
    {
        init_log( "Failed to get physical device surface capabilities.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully retrieved physical device surface capabilities.\n" );
    }

    uint32_t present_mode_count;
    VkPresentModeKHR* present_modes = GetSurfacePresentModesMalloc( (SurfaceQueryInfo) { phys_dev, surface }, &present_mode_count );
    if ( !present_modes )
    {
        init_log( "Failed to get surface present modes." );
        exit( 1 );
    }
    
    // One may query the hardware for supported present modes.
    // However, VK_PRESENT_MODE_FIFO_KHR is required to be supported.
    VkPresentModeKHR selected_present_mode = VK_PRESENT_MODE_FIFO_KHR;

    VkExtent2D swap_extent;
    if ( surface_capabilities.currentExtent.width == 0xFFFFFFFF )
    {
        swap_extent.width = display_width;
        swap_extent.height = display_height;

        if ( swap_extent.width < surface_capabilities.minImageExtent.width )
        {
            swap_extent.width = surface_capabilities.minImageExtent.width;
        }
        else if ( swap_extent.width > surface_capabilities.maxImageExtent.width )
        {
            swap_extent.width = surface_capabilities.maxImageExtent.width;
        }

        if ( swap_extent.height < surface_capabilities.minImageExtent.height )
        {
            swap_extent.height = surface_capabilities.minImageExtent.height;
        }
        else if ( swap_extent.height > surface_capabilities.maxImageExtent.height )
        {
            swap_extent.height = surface_capabilities.maxImageExtent.height;
        }
    }
    else
    {
        swap_extent = surface_capabilities.currentExtent;
    }

    init_log( "Using swapchain extent of (%u, %u)\n", swap_extent.width, swap_extent.height );

    VkSurfaceTransformFlagBitsKHR pretransform;
    if ( surface_capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR )
    {
        pretransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }
    else
    {
        pretransform = surface_capabilities.currentTransform;
    }

    swapchain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchain_create_info.pNext = NULL;
    swapchain_create_info.surface = surface;
    swapchain_create_info.minImageCount = surface_capabilities.minImageCount;
    swapchain_create_info.imageFormat = selected_format;
    swapchain_create_info.imageExtent = swap_extent;
    swapchain_create_info.preTransform = pretransform;
    swapchain_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchain_create_info.imageArrayLayers = 1;
    swapchain_create_info.presentMode = selected_present_mode;
    swapchain_create_info.oldSwapchain = VK_NULL_HANDLE;
    swapchain_create_info.clipped = 1;
    swapchain_create_info.imageColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    swapchain_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchain_create_info.queueFamilyIndexCount = 0;
    swapchain_create_info.pQueueFamilyIndices = NULL;

    uint32_t both_indices[2] = { graphics_queue_index, present_queue_index };
    if ( graphics_queue_index != present_queue_index )
    {
        swapchain_create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swapchain_create_info.queueFamilyIndexCount = 2;
        swapchain_create_info.pQueueFamilyIndices = both_indices;
    }

    check = vkCreateSwapchainKHR( dev, &swapchain_create_info, NULL, &swapchain->swapchain );
    if ( check )
    {
        init_log( "Failed to create a swapchain.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created a swapchain.\n" );
    }

    swapchain->images = GetSwapchainImagesMalloc( (SwapchainQueryInfo) { dev, swapchain->swapchain }, &swapchain->count );
    if ( !swapchain->images )
    {
        init_log( "Failed to get swapchain images.\n" );
        return 1;
    }

    swapchain->views = malloc( sizeof( *swapchain->views ) * swapchain->count );
    if ( !swapchain->views )
    {
        init_log( "Failed to allocate image views.\n" );
        free( swapchain->images );
        return 1;
    }

    VkImageViewCreateInfo image_view_create;
    image_view_create.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_create.pNext = NULL;
    image_view_create.flags = 0;
    image_view_create.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_create.format = selected_format;
    image_view_create.components.r = VK_COMPONENT_SWIZZLE_R;
    image_view_create.components.g = VK_COMPONENT_SWIZZLE_G;
    image_view_create.components.b = VK_COMPONENT_SWIZZLE_B;
    image_view_create.components.a = VK_COMPONENT_SWIZZLE_A;
    image_view_create.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_view_create.subresourceRange.baseMipLevel = 0;
    image_view_create.subresourceRange.levelCount = 1;
    image_view_create.subresourceRange.baseArrayLayer = 0;
    image_view_create.subresourceRange.layerCount = 1;
    uint32_t i;
    for ( i = 0; i < swapchain->count; i++ )
    {
        image_view_create.image = swapchain->images[i];
        check = vkCreateImageView( dev, &image_view_create, NULL, swapchain->views + i );
        if ( check )
        {
            init_log( "Failed to create image view %d.\n", i );
            free( swapchain->images );
            free( swapchain->views );
            return 1;
        }
        else
        {
            init_log( "Successfully created image view %d.\n", i );
        }
    }

    free( present_modes );
    
    return 0;
}

// Creates the graphics and present device queues. These may be the
// same queue. Requires device to be initialized first.
int InitializeGraphicsAndPresentQueues( const VkDevice dev, 
                                        uint32_t graphics_queue_index, 
                                        uint32_t present_queue_index,
                                        VkQueue* graphics_queue,
                                        VkQueue* present_queue )
{
    vkGetDeviceQueue( dev, graphics_queue_index, 0, graphics_queue );
    if ( graphics_queue_index == present_queue_index )
    {
        *present_queue = *graphics_queue;
    }
    else
    {
        vkGetDeviceQueue( dev, present_queue_index, 0, present_queue );
    }

    return 0;
}


int InitializePipelineLayoutAndDescriptorSets( const VkDevice dev,
                                               VkDescriptorSetLayout* descriptor_layout,
                                               VkDescriptorPool* descriptor_pool,
                                               VkDescriptorSet* descriptor_set,
                                               VkPipelineLayout* pipeline_layout )
{
    VkDescriptorSetLayoutBinding layout_bindings[3] = { 0 };
    layout_bindings[0].binding = 0;
    layout_bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    layout_bindings[0].descriptorCount = 1;
    layout_bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    layout_bindings[0].pImmutableSamplers = NULL;

    layout_bindings[1].binding = 1;
    layout_bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    layout_bindings[1].descriptorCount = 1;
    layout_bindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    layout_bindings[1].pImmutableSamplers = NULL;

    layout_bindings[2].binding = 2;
    layout_bindings[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    layout_bindings[2].descriptorCount = 1;
    layout_bindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layout_bindings[2].pImmutableSamplers = NULL;

    VkDescriptorSetLayoutCreateInfo layout_create = { 0 };
    layout_create.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layout_create.pNext = NULL;
    layout_create.bindingCount = SIZE_ARRAY( layout_bindings );
    layout_create.pBindings = layout_bindings;
    
    VkResult check = vkCreateDescriptorSetLayout( dev, &layout_create, NULL, descriptor_layout );
    if ( check )
    {
        init_log( "Failed to create descriptor set layout.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created descriptor set layout.\n" );
    }

    VkPipelineLayoutCreateInfo pipeline_layout_create = { 0 };
    pipeline_layout_create.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_create.pNext = NULL;
    pipeline_layout_create.pushConstantRangeCount = 0;
    pipeline_layout_create.pPushConstantRanges = NULL;
    pipeline_layout_create.setLayoutCount = 1;
    pipeline_layout_create.pSetLayouts = descriptor_layout;

    check = vkCreatePipelineLayout( dev, &pipeline_layout_create, NULL, pipeline_layout );
    if ( check )
    {
        init_log( "Failed to create pipeline layout!\n" );
        exit( 1 );
    }
    else
    {
        init_log( "Successfully created pipeline layout.\n" );
    }

    VkDescriptorPoolSize descriptor_pool_sizes[3];
    descriptor_pool_sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    descriptor_pool_sizes[0].descriptorCount = 1;
    descriptor_pool_sizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_pool_sizes[1].descriptorCount = 1;
    descriptor_pool_sizes[2].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptor_pool_sizes[2].descriptorCount = 1;

    VkDescriptorPoolCreateInfo descriptor_pool_create = { 0 };
    descriptor_pool_create.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_create.pNext = NULL;
    descriptor_pool_create.maxSets = 1;
    descriptor_pool_create.poolSizeCount = SIZE_ARRAY( descriptor_pool_sizes );
    descriptor_pool_create.pPoolSizes = descriptor_pool_sizes;

    check = vkCreateDescriptorPool( dev, &descriptor_pool_create, NULL, descriptor_pool );
    if ( check )
    {
        init_log( "Failed to create descriptor pool.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created descriptor pool.\n" );
    }

    VkDescriptorSetAllocateInfo descriptor_set_alloc;
    descriptor_set_alloc.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_set_alloc.pNext = NULL;
    descriptor_set_alloc.descriptorPool = *descriptor_pool;
    descriptor_set_alloc.descriptorSetCount = 1;
    descriptor_set_alloc.pSetLayouts = descriptor_layout;

    check = vkAllocateDescriptorSets( dev, &descriptor_set_alloc, descriptor_set );
    if ( check )
    {
        init_log( "Failed to allocate descriptor sets.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully allocated descriptor sets.\n" );
    }

    return 0;
}

// Creates a render pass and its subpasses.
int InitializeRenderPass( const VkDevice dev,
                          const VulkanImage* depth_image,
                          VkFormat present_format,
                          VkRenderPass* render_pass )
{
    VkAttachmentDescription attachments[2];
    attachments[0].format = present_format;
    attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    attachments[0].flags = 0;

    attachments[1].format = depth_image->format;
    attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    attachments[1].flags = 0;

    VkAttachmentReference color_reference = { 0 };
    color_reference.attachment = 0;
    color_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depth_reference = { 0 };
    depth_reference.attachment = 1;
    depth_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass = { 0 };
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.flags = 0;
    subpass.inputAttachmentCount = 0;
    subpass.pInputAttachments = NULL;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &color_reference;
    subpass.pResolveAttachments = NULL;
    subpass.pDepthStencilAttachment = &depth_reference;
    subpass.preserveAttachmentCount = 0;
    subpass.pPreserveAttachments = NULL;

    VkRenderPassCreateInfo render_pass_create = { 0 };
    render_pass_create.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    render_pass_create.pNext = NULL;
    render_pass_create.attachmentCount = 2;
    render_pass_create.pAttachments = attachments;
    render_pass_create.subpassCount = 1;
    render_pass_create.pSubpasses = &subpass;
    render_pass_create.dependencyCount = 0;
    render_pass_create.pDependencies = NULL;
    VkResult check = vkCreateRenderPass( dev, &render_pass_create, NULL, render_pass );
    if ( check )
    {
        init_log( "Failed to create render pass.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created render pass.\n" );
    }

    return 0;
}

// Initializes one framebuffer for every image in the swapchain.
int InitializeFramebuffers( const VkDevice dev,
                            const VkRenderPass render_pass, 
                            const VulkanImage* depth_image, 
                            const VulkanSwapchain* swapchain,
                            uint32_t width,
                            uint32_t height,
                            VkFramebuffer* framebuffers )
{
    VkImageView view_attachments[2];
    view_attachments[1] = depth_image->view;

    VkFramebufferCreateInfo framebuffer_create;
    framebuffer_create.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebuffer_create.pNext = NULL;
    framebuffer_create.renderPass = render_pass;
    framebuffer_create.attachmentCount = 2;
    framebuffer_create.pAttachments = view_attachments;
    framebuffer_create.width = width;
    framebuffer_create.height = height;
    framebuffer_create.layers = 1;
    
    uint32_t i;
    for ( i = 0; i < swapchain->count; i++ )
    {
        view_attachments[0] = swapchain->views[i];
        VkResult check = vkCreateFramebuffer( dev, &framebuffer_create, NULL, framebuffers + i );
        if ( check )
        {
            init_log( "Failed to create framebuffer %d.\n", i );
            return 1;
        }
        else
        {
            init_log( "Successfully created framebuffer %d.\n", i );
        }
    }

    return 0;
}

int InitializePipeline( const VkDevice dev,
                        const VkPipelineLayout pipeline_layout,
                        const VkRenderPass render_pass,
                        const VulkanShader* shaders,
                        uint32_t shader_count,
                        VkPipeline* pipeline )
{
    VkDynamicState dynamic_state_enables[VK_DYNAMIC_STATE_RANGE_SIZE] = { 0 };
    VkPipelineDynamicStateCreateInfo dynamic_state_create = { 0 };
    dynamic_state_create.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamic_state_create.pNext = NULL;
    dynamic_state_create.pDynamicStates = dynamic_state_enables;
    dynamic_state_create.dynamicStateCount = 0;

    VkVertexInputBindingDescription vertex_input_binding;
    VkVertexInputAttributeDescription vertex_binding_attributes[3];
    vertex_input_binding.binding = 0;
    vertex_input_binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    vertex_input_binding.stride = sizeof( float ) * 10;
    vertex_binding_attributes[0].binding = 0;
    vertex_binding_attributes[0].location = 0;
    vertex_binding_attributes[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
    vertex_binding_attributes[0].offset = 0;
    vertex_binding_attributes[1].binding = 0;
    vertex_binding_attributes[1].location = 1;
    vertex_binding_attributes[1].format = VK_FORMAT_R32G32_SFLOAT;
    vertex_binding_attributes[1].offset = sizeof( float ) * 4;
    vertex_binding_attributes[2].binding = 0;
    vertex_binding_attributes[2].location = 2;
    vertex_binding_attributes[2].format = VK_FORMAT_R32G32B32A32_SFLOAT;
    vertex_binding_attributes[2].offset = sizeof( float ) * 6;

    VkPipelineVertexInputStateCreateInfo vertex_input_state_create = { 0 };
    vertex_input_state_create.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertex_input_state_create.pNext = NULL;
    vertex_input_state_create.flags = 0;
    vertex_input_state_create.vertexBindingDescriptionCount = 1;
    vertex_input_state_create.pVertexBindingDescriptions = &vertex_input_binding;
    vertex_input_state_create.vertexAttributeDescriptionCount = SIZE_ARRAY( vertex_binding_attributes );
    vertex_input_state_create.pVertexAttributeDescriptions = vertex_binding_attributes;

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create = { 0 };
    input_assembly_state_create.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    input_assembly_state_create.pNext = NULL;
    input_assembly_state_create.primitiveRestartEnable = VK_FALSE;
    input_assembly_state_create.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    VkPipelineRasterizationStateCreateInfo raster_create = { 0 };
    raster_create.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    raster_create.pNext = NULL;
    raster_create.flags = 0;
    raster_create.polygonMode = VK_POLYGON_MODE_FILL;
    raster_create.cullMode = VK_CULL_MODE_BACK_BIT;
    raster_create.frontFace = VK_FRONT_FACE_CLOCKWISE;
    raster_create.depthClampEnable = VK_FALSE;
    raster_create.rasterizerDiscardEnable = VK_FALSE;
    raster_create.depthBiasEnable = VK_FALSE;
    raster_create.depthBiasConstantFactor = 0;
    raster_create.depthBiasClamp = 0;
    raster_create.depthBiasSlopeFactor = 0;
    raster_create.lineWidth = 1.0f;

    VkPipelineColorBlendAttachmentState blend_attach_state[1] = { 0 };
    blend_attach_state[0].colorWriteMask = 0xF;
    blend_attach_state[0].blendEnable = VK_FALSE;
    blend_attach_state[0].alphaBlendOp = VK_BLEND_OP_ADD;
    blend_attach_state[0].colorBlendOp = VK_BLEND_OP_ADD;
    blend_attach_state[0].srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_attach_state[0].dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_attach_state[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_attach_state[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;

    VkPipelineColorBlendStateCreateInfo color_blend_create = { 0 };
    color_blend_create.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blend_create.pNext = NULL;
    color_blend_create.flags = 0;
    color_blend_create.attachmentCount = 1;
    color_blend_create.pAttachments = blend_attach_state;
    color_blend_create.logicOpEnable = VK_FALSE;
    color_blend_create.logicOp = VK_LOGIC_OP_NO_OP;
    color_blend_create.blendConstants[0] = 1.0f;
    color_blend_create.blendConstants[1] = 1.0f;
    color_blend_create.blendConstants[2] = 1.0f;
    color_blend_create.blendConstants[3] = 1.0f;

    VkPipelineViewportStateCreateInfo viewport_create = { 0 };
    viewport_create.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_create.pNext = NULL;
    viewport_create.flags = 0;
    viewport_create.viewportCount = 1;
    dynamic_state_enables[dynamic_state_create.dynamicStateCount++] = VK_DYNAMIC_STATE_VIEWPORT;
    viewport_create.scissorCount = 1;
    dynamic_state_enables[dynamic_state_create.dynamicStateCount++] = VK_DYNAMIC_STATE_SCISSOR;
    viewport_create.pViewports = NULL;
    viewport_create.pScissors = NULL;

    VkPipelineDepthStencilStateCreateInfo depth_stencil_create = { 0 };
    depth_stencil_create.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depth_stencil_create.pNext = NULL;
    depth_stencil_create.flags = 0;
    depth_stencil_create.depthTestEnable = VK_TRUE;
    depth_stencil_create.depthWriteEnable = VK_TRUE;
    depth_stencil_create.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    depth_stencil_create.depthBoundsTestEnable = VK_FALSE;
    depth_stencil_create.minDepthBounds = 0;
    depth_stencil_create.maxDepthBounds = 0;
    depth_stencil_create.stencilTestEnable = VK_FALSE;
    depth_stencil_create.back.failOp = VK_STENCIL_OP_KEEP;
    depth_stencil_create.back.passOp = VK_STENCIL_OP_KEEP;
    depth_stencil_create.back.compareOp = VK_COMPARE_OP_ALWAYS;
    depth_stencil_create.back.compareMask = 0;
    depth_stencil_create.back.reference = 0;
    depth_stencil_create.back.depthFailOp = VK_STENCIL_OP_KEEP;
    depth_stencil_create.back.writeMask = 0;
    depth_stencil_create.front = depth_stencil_create.back;

    VkPipelineMultisampleStateCreateInfo multisample_create = { 0 };
    multisample_create.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisample_create.pNext = NULL;
    multisample_create.flags = 0;
    multisample_create.pSampleMask = NULL;
    multisample_create.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisample_create.sampleShadingEnable = VK_FALSE;
    multisample_create.alphaToCoverageEnable = VK_FALSE;
    multisample_create.alphaToOneEnable = VK_FALSE;
    multisample_create.minSampleShading = 0.0;

    VkPipelineShaderStageCreateInfo* shader_create = malloc( sizeof(*shader_create) * shader_count );
    uint32_t i;
    for ( i = 0; i < shader_count; i++ )
    {
        shader_create[i] = shaders[i].stage_create;
    }

    VkGraphicsPipelineCreateInfo pipeline_create = { 0 };
    pipeline_create.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_create.pNext = NULL;
    pipeline_create.layout = pipeline_layout;
    pipeline_create.basePipelineHandle = VK_NULL_HANDLE;
    pipeline_create.basePipelineIndex = 0;
    pipeline_create.flags = 0;
    pipeline_create.pVertexInputState = &vertex_input_state_create;
    pipeline_create.pInputAssemblyState = &input_assembly_state_create;
    pipeline_create.pRasterizationState = &raster_create;
    pipeline_create.pColorBlendState = &color_blend_create;
    pipeline_create.pTessellationState = NULL;
    pipeline_create.pMultisampleState = &multisample_create;
    pipeline_create.pDynamicState = &dynamic_state_create;
    pipeline_create.pViewportState = &viewport_create;
    pipeline_create.pDepthStencilState = &depth_stencil_create;
    pipeline_create.pStages = shader_create;
    pipeline_create.stageCount = shader_count;
    pipeline_create.renderPass = render_pass;
    pipeline_create.subpass = 0;

    VkResult check = vkCreateGraphicsPipelines( dev, VK_NULL_HANDLE, 1, &pipeline_create, NULL, pipeline );
    
    free( shader_create );
    
    if ( check )
    {
        init_log( "Failed to create the graphics pipeline.\n" );
        return 1;
    }
    else
    {
        init_log( "Successfully created the graphics pipeline.\n" );
    }

    return 0;
}
