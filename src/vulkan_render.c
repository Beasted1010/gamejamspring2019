#include "stdio.h"
#include "stdlib.h"

//#define LOG_LEVEL LOG_VERBOSE
#include "vulkan_wrapper.h"

// Internally acquires a swapchain image and provides a semaphore for
// waiting for its successful acquisition.
int AcquireSwapchainImage( const VkDevice dev, 
                           const VulkanSwapchain* swapchain, 
                           uint32_t* framebuffer_index,
                           VkSemaphore* acquire_sem )
{
    VkSemaphoreCreateInfo acquire_sem_create = { 0 };
    acquire_sem_create.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    acquire_sem_create.pNext = NULL;
    acquire_sem_create.flags = 0;

    VkResult check = vkCreateSemaphore( dev, &acquire_sem_create, NULL, acquire_sem );
    if ( check )
    {
        render_log( "Failed to create image acquired semaphore.\n" );
        return 1;
    }
    else
    {
        render_log( "Successfully created image acquired semaphore.\n" );
    }

    check = vkAcquireNextImageKHR( dev, swapchain->swapchain, UINT64_MAX, *acquire_sem, VK_NULL_HANDLE, framebuffer_index );
    if ( check )
    {
        render_log( "Failed to acquire next image.\n" );
        return 1;
    }
    else
    {
        render_log( "Successfully acquired next image.\n" );
    }

    return 0;
}

int BeginCommandBuffer( const VkCommandBuffer commands )
{
    VkCommandBufferBeginInfo cmd_buf_begin = { 0 };
    cmd_buf_begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmd_buf_begin.pNext = NULL;
    cmd_buf_begin.flags = 0;
    cmd_buf_begin.pInheritanceInfo = NULL;

    VkResult check = vkBeginCommandBuffer( commands, &cmd_buf_begin );
    if ( check )
    {
        render_log( "Failed to begin command buffer.\n" );
        return 1;
    }
    else
    {
        render_log( "Successfully began command buffer.\n" );
    }

    return 0;
}
                        

int BeginRenderRecording( const VkDevice dev,
                          const VkRenderPass render_pass,
                          const VkFramebuffer framebuffer,
                          const VkCommandBuffer commands,
                          RenderRect render_area )
{
    VkClearValue clear_values[2];
    clear_values[0].color.float32[0] = 0.2f;
    clear_values[0].color.float32[1] = 0.2f;
    clear_values[0].color.float32[2] = 0.2f;
    clear_values[0].color.float32[3] = 0.2f;
    clear_values[1].depthStencil.depth = 1.0f;
    clear_values[1].depthStencil.stencil = 0;

    VkRenderPassBeginInfo render_begin = { 0 };
    render_begin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_begin.pNext = NULL;
    render_begin.renderPass = render_pass;
    render_begin.framebuffer = framebuffer;
    render_begin.renderArea.offset.x = render_area.x;
    render_begin.renderArea.offset.y = render_area.y;
    render_begin.renderArea.extent.width = render_area.width;
    render_begin.renderArea.extent.height = render_area.height;
    render_begin.clearValueCount = 2;
    render_begin.pClearValues = clear_values;

    BeginCommandBuffer( commands );

    vkCmdBeginRenderPass( commands, &render_begin, VK_SUBPASS_CONTENTS_INLINE );

    return 0;
}

int EndCommandBuffer( const VkCommandBuffer commands )
{
    VkResult check = vkEndCommandBuffer( commands );
    if ( check )
    {
        render_log( "Failed to end command buffer.\n" );
        return 1;
    }
    else
    {
        render_log( "Successfully ended command buffer.\n" );
    }

    return 0;
}

// Uses an input semaphore to wait for a given image to be acquired,
// at which point the queue is submitted and a fence is provided for
// waiting for presentation.
int SubmitRenderQueue( const VkDevice dev,
                       const VkQueue graphics_queue,
                       const VkCommandBuffer* command_buffers,
                       uint32_t buffer_count,
                       const VkSemaphore* image_acquired, 
                       VkFence* draw_fence )
{
    VkFenceCreateInfo fence_create;
    fence_create.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_create.pNext = NULL;
    fence_create.flags = 0;
    vkCreateFence( dev, &fence_create, NULL, draw_fence );

    VkPipelineStageFlags pipeline_stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    VkSubmitInfo submit_info[1] = { 0 };
    submit_info[0].sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info[0].pNext = NULL;
    submit_info[0].waitSemaphoreCount = 1;
    submit_info[0].pWaitSemaphores = image_acquired;
    submit_info[0].pWaitDstStageMask = &pipeline_stage_flags;
    submit_info[0].commandBufferCount = buffer_count;
    submit_info[0].pCommandBuffers = command_buffers;
    submit_info[0].signalSemaphoreCount = 0;
    submit_info[0].pSignalSemaphores = NULL;

    VkResult check = vkQueueSubmit( graphics_queue, 1, submit_info, *draw_fence );
    if ( check )
    {
        render_log( "Failed to submit queue.\n" );
        return 1;
    }
    else
    {
        render_log( "Successfully submitted queue.\n" );
    }

    return 0;
}

int SubmitBasicQueue( VkQueue queue,
                      const VkCommandBuffer command_buffer )
{
    VkSubmitInfo submit_info = {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;

    VkResult res = vkQueueSubmit( queue, 1, &submit_info, VK_NULL_HANDLE );
    vkQueueWaitIdle( queue );

    return res;
}

// If non-NULL, waits for the given fence to complete and then
// presents the created image to the display.
int PresentToDisplay( const VkDevice dev,
                      const VkQueue present_queue,
                      const VulkanSwapchain* swapchain,
                      uint32_t buffer_index,
                      const VkFence draw_fence )
{
    VkPresentInfoKHR present_info;
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.pNext = NULL;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &swapchain->swapchain;
    present_info.pImageIndices = &buffer_index;
    present_info.pWaitSemaphores = NULL;
    present_info.waitSemaphoreCount = 0;
    present_info.pResults = 0;

    VkResult check;
    if ( draw_fence )
    {
        WaitForFenceIndefinite( dev, draw_fence );
    }
    check = vkQueuePresentKHR( present_queue, &present_info );

    if ( check )
    {
        render_log( "Could not present to display." );
        return 1;
    }
    else
    {
        render_log( "Successfully presented." );
    }

    return 0;
}
