#include "common.h"
#include "stdio.h"
#include "stdlib.h"

//#define LOG_LEVEL LOG_QUIET
#include "vulkan_wrapper.h"
#include "listmalloc.h"

void WaitForFenceIndefinite( const VkDevice dev,
                             const VkFence wait_for )
{
    VkResult check;
    do
    {
        // Timeout is in nanoseconds.
        check = vkWaitForFences( dev, 1, &wait_for, VK_TRUE, (uint64_t)1e9 );
    } while ( check == VK_TIMEOUT );
}

void CreateDefaultSampler( const VkDevice dev,
                           VkSampler* sampler )
{
    VkSamplerCreateInfo sampler_info = { 0 };
    sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_info.magFilter = VK_FILTER_LINEAR;
    sampler_info.minFilter = VK_FILTER_LINEAR;
    sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_info.anisotropyEnable = VK_TRUE;
    sampler_info.maxAnisotropy = 16;
    sampler_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    sampler_info.unnormalizedCoordinates = VK_FALSE;
    sampler_info.compareEnable = VK_FALSE;
    sampler_info.compareOp = VK_COMPARE_OP_ALWAYS;
    sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    sampler_info.mipLodBias = 0.0f;
    sampler_info.minLod = 0.0f;
    sampler_info.maxLod = 0.0f;

    VkResult res = vkCreateSampler( dev, &sampler_info, NULL, sampler );

    if ( res != VK_SUCCESS )
    {
        printf( "Error creating the default sampler!" );
        exit( 1 );
    }
}

int GetMemoryTypeFromDeviceProperties( const VkPhysicalDeviceMemoryProperties* properties, 
                                       uint32_t type_bits,
                                       VkFlags req_mask, 
                                       uint32_t* type_index )
{
    // Search memtypes to find first index with those properties
    for ( uint32_t i = 0; i < properties->memoryTypeCount; i++ )
    {
        if ( (type_bits & 1) == 1 )
        {
            // Type is available, does it match user properties?
            if ( (properties->memoryTypes[i].propertyFlags & req_mask) == req_mask )
            {
                *type_index = i;
                return 0;
            }
        }
        type_bits >>= 1;
    }

    // No memory types matched, return failure
    return 1;
}

void UpdateSamplerDescriptorSet( const VkDevice dev,
                                 const VkDescriptorSet descriptor_set,
                                 const VulkanImage* image,
                                 const VkSampler sampler,
                                 uint32_t destination_binding )
{
    VkDescriptorImageInfo image_info = { 0 };
    image_info.imageLayout = image->layout;
    image_info.imageView = image->view;
    image_info.sampler = sampler;

    VkWriteDescriptorSet writes[1] = { 0 };
    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].pNext = NULL;
    writes[0].dstSet = descriptor_set;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[0].pImageInfo = &image_info;
    writes[0].pBufferInfo = NULL;
    writes[0].dstArrayElement = 0;
    writes[0].dstBinding = destination_binding;

    vkUpdateDescriptorSets( dev, 1, writes, 0, NULL );
}

void UpdateUniformDescriptorSet( const VkDevice dev, 
                                 const VkDescriptorSet descriptor_set, 
                                 const UniformBuffer* uniform_buf,
                                 uint32_t destination_binding )
{
    VkWriteDescriptorSet writes[1] = { 0 };
    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].pNext = NULL;
    writes[0].dstSet = descriptor_set;
    writes[0].descriptorCount = 1;
    writes[0].descriptorType = uniform_buf->is_dynamic ? VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC : VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writes[0].pBufferInfo = &uniform_buf->data.descriptor_info;
    writes[0].dstArrayElement = 0;
    writes[0].dstBinding = destination_binding;

    vkUpdateDescriptorSets( dev, 1, writes, 0, NULL );
}

static VkDebugReportCallbackEXT debug_callback;
static PFN_vkCreateDebugReportCallbackEXT create_callback = NULL;
static PFN_vkDestroyDebugReportCallbackEXT destroy_callback = NULL;


static VkBool32 ReportCallback( VkDebugReportFlagsEXT flags,
                                VkDebugReportObjectTypeEXT object_type,
                                uint64_t object,
                                size_t location,
                                int32_t message_code,
                                const char* layer_prefix,
                                const char* message,
                                void* user_data )
{    
    const char* message_type;
    if ( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT )
    {
        message_type =  "ERROR";
    }
    else if ( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT )
    {
        message_type = "WARNING";
    }
    else if ( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT )
    {
        message_type = "PERFORMANCE WARNING";
    }
    else if ( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT )
    {
        message_type = "INFORMATION";
    }
    else
    {
        message_type = "DEBUG";
    }

    printf( "%s: Layer %s - '%s' (%zu)\n", message_type, layer_prefix, message, location );
    
    return VK_TRUE;
}

void InitializeDebugReporter( VkInstance instance )
{
    if ( create_callback == NULL )
    {
        create_callback = (PFN_vkCreateDebugReportCallbackEXT)
            vkGetInstanceProcAddr( instance, "vkCreateDebugReportCallbackEXT" );
    }
    
    VkDebugReportCallbackCreateInfoEXT callback;
    callback.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    callback.pNext = NULL;
    callback.flags =
        VK_DEBUG_REPORT_ERROR_BIT_EXT |
        VK_DEBUG_REPORT_WARNING_BIT_EXT |
        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
        VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
        VK_DEBUG_REPORT_DEBUG_BIT_EXT;

    callback.pfnCallback = ReportCallback;
    callback.pUserData = NULL;

    VkResult res = create_callback( instance, &callback, NULL, &debug_callback );
    if ( res != VK_SUCCESS )
    {
        init_log( "Failed to create debug report callback." );
        exit( 1  );
    }
}

void DestroyDebugReporter( VkInstance instance )
{
    if ( destroy_callback == NULL )
    {
        destroy_callback = (PFN_vkDestroyDebugReportCallbackEXT)
            vkGetInstanceProcAddr( instance, "vkDestroyDebugReportCallbackEXT" );
    }
    
    destroy_callback( instance, debug_callback, NULL );
}

