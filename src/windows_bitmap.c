#include "common.h"
#include "stdio.h"
#include "windows.h"

#include "platform.h"

#define BITS_PER_PIXEL 32
#define TRANSPARENT_MASK 0xFF000000

typedef struct Bitmap
{
    BITMAPINFO info;
    uint32_t w, h;
    void* pixels;
} Bitmap;

typedef struct BMPFileHeader
{
    char type[2];
    uint32_t fsize;

    // NOTE: in the file header, there are two
    // reserved words between fsize and bit_start.
    // We do not need to read these words.

    uint32_t bit_start;
} BMPFileHeader;

typedef struct BMPMask
{
    uint32_t rmask, gmask, bmask, amask;
    uint32_t rshift, gshift, bshift, ashift;
    uint32_t rmax, gmax, bmax, amax;
} BMPMask;

typedef struct BMP3Header
{
    uint32_t header_size;
    int32_t w, h;
    uint16_t planes;
    uint16_t bits_pp;
    uint32_t compression;
    uint32_t bmpsize;

    // These values are in pixels per meter,
    // so aren't very useful for our purposes.
    // We will use the w and h instead.
    int32_t hres, vres;

    uint32_t col_used;
    uint32_t col_important;
    BMPMask bitmask;
} BMP3Header;

static int ResizeImageMemory( Bitmap* img, uint32_t w, uint32_t h );

static int CreateImage( Bitmap* img, uint32_t w, uint32_t h )
{
    img->pixels = 0;
    img->w = 0;
    img->h = 0;
    img->info.bmiHeader.biSize = sizeof( img->info.bmiHeader );
    img->info.bmiHeader.biPlanes = 1;
    img->info.bmiHeader.biBitCount = BITS_PER_PIXEL;
    img->info.bmiHeader.biCompression = BI_RGB;

    return ResizeImageMemory( img, w, h );
}

static int ResizeImageMemory( Bitmap* img, uint32_t w, uint32_t h )
{
    if ( img->pixels )
    {
        VirtualFree( img->pixels, 0, MEM_RELEASE );
    }

    img->info.bmiHeader.biWidth = w;
    img->info.bmiHeader.biHeight = -h;
    img->w = w;
    img->h = h;

    uint32_t image_size = BITS_PER_PIXEL * img->w * img->h;
    if ( image_size == 0 )
    {
        MessageBoxA( 0, "Cannot allocate 0 size pixel memory.", 0, MB_OK );
    }

    img->info.bmiHeader.biSizeImage = image_size;

    img->pixels = VirtualAlloc( NULL, image_size, MEM_COMMIT, PAGE_READWRITE );
    if ( !img->pixels )
    {
        MessageBoxA( 0, "Failed to allocate pixel memory.", 0, MB_OK );
        return 0;
    }
    else
    {
        memset( img->pixels, 0, image_size );
    }

    return 1;
}

// PIXEL-LEVEL RENDERING
static inline void WriteRGB( uint32_t* towrite, uint8_t r, uint8_t g, uint8_t b )
{
    // X R G B      X R G B     X R G B     X R G B
    // X R G B      X R G B     X R G B     X R G B
    // X R G B      X R G B<-dt X R G B     X R G B
    // X R G B      X R G B     X R G B     X R G B
    // X R G B      X R G B     X R G B     X R G B ...
    //
    // Want: 1, 2
    // Example: WriteRGB( img->pixels + (img->w (4) * 2 + 1) * 4(bytes_per_pixel), 128, 16, 32 )
    // Each pixel is a uint32_t
    // 0x00RRGGBB == 0x00RR0000 | 0x0000GG00 | 0x000000BB == (0x000000RR << 16) | (0x000000GG << 8) | 0x000000BB
    // In this case, 0x000000RR == 0x000000F0, 0x000000GG == 0x00000010, 0x000000BB == 0x00000020

    *towrite = (r << 16) | (g << 8) | b;
}

static inline void WriteRGBA( uint32_t* towrite, uint8_t r, uint8_t g, uint8_t b, uint8_t a )
{
    *towrite = (a << 24) | (r << 16) | (g << 8) | b;
}

// If mask == 0, returns 0 and leaves index unchanged.
// If mask > 0, returns 1 and index is filled with the
// index of the least significant 1-bit of mask.
// See MSDN reference: https://msdn.microsoft.com/en-us/library/fbxyd7zd.aspx
// See GCC reference: https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html
static int ReverseBitScan( uint32_t* index, uint32_t mask )
{
    #ifdef MSVC
        // Untested
        #include "intrin.h"
        #pragma intrinsic( _BitScanReverse )
        return _BitScanReverse( index, mask );
    #else
        int res = __builtin_ffs( mask );
        if ( res == 0 )
        {
            return 0;
        }
        else
        {
            *index = res - 1;
            return 1;
        }
    #endif
}

static uint16_t SwapByteOrder16( uint16_t x )
{
    return ((x & 0xFF00) >> 8) |
           ((x & 0x00FF) << 8);
}

static uint32_t SwapByteOrder32( uint32_t x )
{
    return ((x & 0xFF000000) >> 24) |
           ((x & 0x00FF0000) >> 8)  |
           ((x & 0x0000FF00) << 8)  |
           ((x & 0x000000FF) << 24);
}

// Quick define to read a type from a stream of bytes.
// For our purposes, we are assuming the byte stream's endianness
// matches the native little-endian architecture (on x86 and x64)
// The macro reads a value of size sizeof(type) from the stream,
// and increments the offset so that this function may be called
// repeatedly.
#define READ_BYTESTREAM( type, ptr, off ) (*(type*)(ptr + off)); off += sizeof(type);

Bitmap* LoadBitmapFromFile( const char* filename, const RGB* colorkey )
{
    // Reference for this function:
    // http://www.fileformat.info/format/bmp/egff.htm
    // What we support:
    // v3 Windows NT format for:
    //    - 24 bit RGB           (Compression = 0)
    //    - 16-bit with bitmasks (Compression = 3)
    //    - 32-bit with bitmasks (Compression = 3)
    // v4.x BMPs that are 16, 24, or 32-bit should
    // generally work.
    // Previous versions of the BMP file format are not supported.
    // We do not support RLE compression or any other
    // bit size for pixels.

    int returncode = 0;
    HANDLE file;
    char* pixelbuf = NULL;
    Bitmap* img = VirtualAlloc( NULL, sizeof( Bitmap ), MEM_COMMIT, PAGE_READWRITE );

    uint32_t mapped_colorkey;
    if ( colorkey )
    {
        WriteRGB( &mapped_colorkey, colorkey->r, colorkey->g, colorkey->b );
    }

// Bracket to prevent scope errors
{
    // First, clear the given image
    img->pixels = 0;
    img->w = 0;
    img->h = 0;

    // Reference: https://msdn.microsoft.com/en-us/library/windows/desktop/bb540534(v=vs.85).aspx
    file = CreateFile(filename,              // file to open
                      GENERIC_READ,          // open for reading
                      FILE_SHARE_READ,       // share for reading
                      NULL,                  // default security
                      OPEN_EXISTING,         // existing file only
                      FILE_ATTRIBUTE_NORMAL, // normal file
                      NULL);                 // no attr. template
    if ( file == INVALID_HANDLE_VALUE )
    {
        sprintf( error_buf, "Could not open file %s: %lx", filename, GetLastError( ));
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }

    // We first read the 14-byte header for the file
    // To figure out how large the file is.
    const uint32_t head_size = 14;
    char filehead_bytes[head_size];
    DWORD bytes_read = 0;

    // The following is a structure that keeps track of our place in the file.
    OVERLAPPED ol = {0};

    // Read the header
    if ( !ReadFile( file, &filehead_bytes, head_size, &bytes_read, &ol ) ||
                    bytes_read != head_size )
    {
        sprintf( error_buf, "Could not read file header of %s: %lx", filename, GetLastError( ));
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }
    ol.Offset += bytes_read;

    BMPFileHeader filehead;
    uint32_t offset = 0;

    // Read the file type (Should be "BM")
    filehead.type[0] = READ_BYTESTREAM( char, filehead_bytes, offset );
    filehead.type[1] = READ_BYTESTREAM( char, filehead_bytes, offset );
    if ( filehead.type[0] != 'B' || filehead.type[1] != 'M' )
    {
        sprintf( error_buf, "File type of %s does not match \"BM\"", filename );
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }

    // Read the size of the file
    filehead.fsize = READ_BYTESTREAM( DWORD, filehead_bytes, offset );

    // After the file size are two reserved WORDs
    offset += 2 * sizeof(WORD);

    // Read the location that image data begins.
    filehead.bit_start = READ_BYTESTREAM( DWORD, filehead_bytes, offset )

    offset = 0;

    BMP3Header bmphead;

    // Assume the BMP header is at least 40 bytes long.
    // If it is longer, that is okay, but if it's shorter,
    // it's a BMP version we do not support.
    const uint32_t bmphead_size = 40;
    char bmphead_bytes[bmphead_size];

    if ( !ReadFile( file, &bmphead_bytes, bmphead_size, &bytes_read, &ol ) ||
                    bytes_read != bmphead_size )
    {
        sprintf( error_buf, "Could not read file header of %s: %lx", filename, GetLastError( ));
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }
    ol.Offset += bytes_read;

    // Read the header size and ensure it is large enough.
    bmphead.header_size = READ_BYTESTREAM( DWORD, bmphead_bytes, offset );
    if ( bmphead.header_size < bmphead_size )
    {
        sprintf( error_buf, "BMP file format version of %s not supported.", filename );
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }
    int is_v4 = (bmphead.header_size > bmphead_size);


    // Read pixel formatting data
    bmphead.w = READ_BYTESTREAM( LONG, bmphead_bytes, offset );
    bmphead.h = READ_BYTESTREAM( LONG, bmphead_bytes, offset );
    bmphead.planes = READ_BYTESTREAM( WORD, bmphead_bytes, offset );
    bmphead.bits_pp = READ_BYTESTREAM( WORD, bmphead_bytes, offset );
    if ( bmphead.bits_pp != 16 &&
         bmphead.bits_pp != 24 &&
         bmphead.bits_pp != 32 )
    {
        sprintf( error_buf, "Bits per pixel of %hu for file %s not supported.",
                 bmphead.bits_pp, filename );
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }

    // Compression value must be 0 if bits_pp = 24, and 3 otherwise.
    bmphead.compression = READ_BYTESTREAM( DWORD, bmphead_bytes, offset );
    if ( bmphead.bits_pp == 24 )
    {
        if ( bmphead.compression != 0 )
        {
            sprintf( error_buf, "Compression of %u (bpp = %hu) for file %s is not supported.",
                     bmphead.compression, bmphead.bits_pp, filename );
            MessageBoxA( 0, error_buf, 0, MB_OK );
            goto stop;
        }
    }
    else if ( bmphead.compression != 3 )
    {
        sprintf( error_buf, "Compression of %u (bpp = %hu) for file %s is not supported.",
                 bmphead.compression, bmphead.bits_pp, filename );
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }

    bmphead.bmpsize = READ_BYTESTREAM( DWORD, bmphead_bytes, offset );

    // These values are in pixels per meter,
    // so aren't very useful for our purposes.
    // We will use the w and h instead.
    bmphead.hres = READ_BYTESTREAM( LONG, bmphead_bytes, offset );
    bmphead.vres = READ_BYTESTREAM( LONG, bmphead_bytes, offset );

    // These values do not really matter to us.
    bmphead.col_used = READ_BYTESTREAM( DWORD, bmphead_bytes, offset );
    bmphead.col_important = READ_BYTESTREAM( DWORD, bmphead_bytes, offset );

    // If there is a compression level of 3,
    // we read the bitmasks for r, g, and b as well.
    if ( bmphead.compression == 3 )
    {
        uint32_t mask_size;
        if ( is_v4 )
        {
            mask_size = 4 * sizeof( DWORD );
        }
        else
        {
            mask_size = 3 * sizeof( DWORD );
        }

        char mask_bytes[4];
        if ( !ReadFile( file, &mask_bytes, mask_size, &bytes_read, &ol ) ||
                        bytes_read != mask_size )
        {
            sprintf( error_buf, "Could not read bitmasks of %s: %lx", filename, GetLastError( ));
            MessageBoxA( 0, error_buf, 0, MB_OK );
            goto stop;
        }
        ol.Offset += bytes_read;

        offset = 0;
        bmphead.bitmask.rmask = READ_BYTESTREAM( DWORD, mask_bytes, offset );
        bmphead.bitmask.gmask = READ_BYTESTREAM( DWORD, mask_bytes, offset );
        bmphead.bitmask.bmask = READ_BYTESTREAM( DWORD, mask_bytes, offset );

        // Get the shift values for each color
        ReverseBitScan( &bmphead.bitmask.rshift, bmphead.bitmask.rmask );
        ReverseBitScan( &bmphead.bitmask.gshift, bmphead.bitmask.gmask );
        ReverseBitScan( &bmphead.bitmask.bshift, bmphead.bitmask.bmask );

        // Get the maximum values for each color
        bmphead.bitmask.rmax = bmphead.bitmask.rmask >> bmphead.bitmask.rshift;
        bmphead.bitmask.gmax = bmphead.bitmask.gmask >> bmphead.bitmask.gshift;
        bmphead.bitmask.bmax = bmphead.bitmask.bmask >> bmphead.bitmask.bshift;

        if ( is_v4 )
        {
            // In V4 Bitmap and above, we have an alpha channel!
            bmphead.bitmask.amask = READ_BYTESTREAM( DWORD, mask_bytes, offset );
            if ( ReverseBitScan( &bmphead.bitmask.ashift, bmphead.bitmask.amask ))
            {
                bmphead.bitmask.amax = bmphead.bitmask.amask >> bmphead.bitmask.ashift;
            }
            else
            {
                bmphead.bitmask.ashift = 0;
                bmphead.bitmask.amax = 0;
            }
        }
        else
        {
            bmphead.bitmask.amask = 0;
            bmphead.bitmask.ashift = 0;
            bmphead.bitmask.amax = 0;
        }
    }

    // Now, it is time to begin actually reading the bitmap data!
    // No matter how the image is stored on disk, we are going to
    // load it into our 32-bit RGB format.

    // The first step is to create the image
    uint32_t realw = bmphead.w < 0 ? -bmphead.w : bmphead.w;
    uint32_t realh = bmphead.h < 0 ? -bmphead.h : bmphead.h;
    if ( !CreateImage( img, realw, realh ))
    {
        MessageBoxA( 0, "Failed to create image.", 0, MB_OK );
        goto stop;
    }

    // If the height of the saved bitmap is positive,
    // it is saved "bottom-up" in the y direction.
    // Since all of our images are saved "top-down",
    // we must convert it if need be.
    int ydir = 1;
    int ystart = 0;
    if ( bmphead.h > 0 )
    {
        ydir = -1;
        ystart = realh - 1;
    }

    uint32_t bytes_pp = bmphead.bits_pp / 8;
    size_t pitch = bytes_pp * realw;
    uint32_t pitchmod = pitch % 4;
    if ( pitchmod )
    {
        // Each scanline is aligned on a 32-bit boundary
        pitch += 4 - pitchmod;
    }

    uint32_t bmp_size = realh * pitch;

    // Allocate a buffer large enough for the entire bitmap data
    pixelbuf = VirtualAlloc( NULL, bmp_size, MEM_COMMIT, PAGE_READWRITE );
    if ( !pixelbuf )
    {
        MessageBoxA( 0, "Failed to allocate pixel buffer.", 0, MB_OK );
        goto stop;
    }

    ol.Offset = filehead.bit_start;
    if ( !ReadFile( file, pixelbuf, bmp_size, &bytes_read, &ol ))
    {
        sprintf( error_buf, "Could not read file contents of %s: %lx", filename, GetLastError( ));
        MessageBoxA( 0, error_buf, 0, MB_OK );
        goto stop;
    }

    int x, y;
    void* read_row = pixelbuf + pitch * ystart;
    void* read_pixel = read_row;
    void* write_pixel = img->pixels;
    for ( y = 0; y < realh; y++ )
    {
        read_pixel = read_row;
        for ( x = 0; x < realw; x++ )
        {
            uint8_t r, g, b, a = 0;
            switch (bmphead.bits_pp)
            {
                case 16:
                {
                    uint16_t pval = *(uint16_t*)read_pixel;

                    // If the bmp is in Windows NT v3 format,
                    // the pixels are stored in a big endian format,
                    // so we must swap their byte orders.
                    if ( bmphead.header_size == head_size )
                    {
                        pval = SwapByteOrder16( pval );
                    }

                    uint16_t rraw = (pval & bmphead.bitmask.rmask) >> bmphead.bitmask.rshift;
                    uint16_t graw = (pval & bmphead.bitmask.gmask) >> bmphead.bitmask.gshift;
                    uint16_t braw = (pval & bmphead.bitmask.bmask) >> bmphead.bitmask.bshift;

                    r = ((uint32_t)rraw * 0xFF) / bmphead.bitmask.rmax;
                    g = ((uint32_t)graw * 0xFF) / bmphead.bitmask.gmax;
                    b = ((uint32_t)braw * 0xFF) / bmphead.bitmask.bmax;

                    if ( is_v4 && bmphead.bitmask.amax )
                    {
                        uint16_t araw = (pval & bmphead.bitmask.amask) >> bmphead.bitmask.ashift;
                        a = ((uint32_t)araw * 0xFF) / bmphead.bitmask.amax;
                    }
                }
                break;

                case 24:
                    r = *(char*)read_pixel;
                    g = *(char*)(read_pixel + 1);
                    b = *(char*)(read_pixel + 2);
                break;

                case 32:
                {                    
                    uint32_t pval = *(uint32_t*)read_pixel;

                    // If the bmp is in Windows NT v3 format,
                    // the pixels are stored in a big endian format,
                    // so we must swap their byte orders.
                    if ( bmphead.header_size == head_size )
                    {
                        pval = SwapByteOrder32( pval );
                    }

                    uint32_t rraw = (pval & bmphead.bitmask.rmask) >> bmphead.bitmask.rshift;
                    uint32_t graw = (pval & bmphead.bitmask.gmask) >> bmphead.bitmask.gshift;
                    uint32_t braw = (pval & bmphead.bitmask.bmask) >> bmphead.bitmask.bshift;

                    r = (rraw * 0xFF) / bmphead.bitmask.rmax;
                    g = (graw * 0xFF) / bmphead.bitmask.gmax;
                    b = (braw * 0xFF) / bmphead.bitmask.bmax;

                    if ( is_v4 && bmphead.bitmask.amax )
                    {
                        uint32_t araw = (pval & bmphead.bitmask.amask) >> bmphead.bitmask.ashift;
                        a = (araw * 0xFF) / bmphead.bitmask.amax;
                    }
                }
                break;

                default:
                break;
            }

            if (is_v4)
            {
                WriteRGBA( write_pixel, r, g, b, a );
                if ( colorkey && mapped_colorkey == *(uint32_t*)write_pixel )
                {
                    *(uint32_t*)write_pixel |= TRANSPARENT_MASK;
                }
            }
            else
            {
                WriteRGB( write_pixel, r, g, b );
                if ( colorkey && mapped_colorkey == *(uint32_t*)write_pixel )
                {
                    *(uint32_t*)write_pixel |= TRANSPARENT_MASK;
                }
            }

            write_pixel += sizeof( uint32_t );
            read_pixel += bytes_pp;
        }
        read_row += ydir * pitch;
    }

    // If we made it this far, we succeeded
    returncode = 1;
}

stop:
    if ( file != INVALID_HANDLE_VALUE )
    {
        CloseHandle( file );
    }

    // If we failed, clear the image again.
    if ( !returncode && img->pixels )
    {
        FreeBitmap( img );
    }

    if ( pixelbuf )
    {
        VirtualFree( pixelbuf, 0, MEM_RELEASE );
    }

    return img;
}

void FreeBitmap( Bitmap* img )
{
    VirtualFree( img->pixels, 0, MEM_RELEASE );
    VirtualFree( img, 0, MEM_RELEASE );
}

void* GetBitmapPixels( const Bitmap* bmp )
{
    return bmp->pixels;
}

uint32_t GetBitmapWidth( const Bitmap* bmp )
{
    return bmp->w;
}

uint32_t GetBitmapHeight( const Bitmap* bmp )
{
    return bmp->h;
}
