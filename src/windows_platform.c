
#include "windows.h"
#include "windowsx.h"
#include "stdio.h"

#include "common.h"
#include "platform.h"

typedef struct WindowInfo
{
    HINSTANCE instance;
    WNDCLASS wnd_class;

    int width, height;
    int win_width, win_height;

    HWND win_handle;
} WindowInfo;

const char* vulkan_instance_extension_names[] =
{
    VK_KHR_SURFACE_EXTENSION_NAME,
    VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
    VK_EXT_DEBUG_REPORT_EXTENSION_NAME
};

const uint32_t vulkan_instance_extension_num = SIZE_ARRAY( vulkan_instance_extension_names );

static WindowInfo window_info;
static struct Input static_input;

//
// This function is a callback, used by Windows whenever a message is received.
// win_handle is the window that the message occurred on,
// and message is an integer describing the message itself.
//
LRESULT CALLBACK WindowProcedure( HWND win_handle, UINT message, WPARAM wparam, LPARAM lparam )
{
    LRESULT result;

    switch ( message )
    {
        // WM_CLOSE is a *command* that tells our window to close.
        // As such, we destroy the window.
        case WM_CLOSE:
            DestroyWindowInfo( &window_info );
            static_input.quit = 1;
            result = 0;
        break;

        // WM_DESTROY is a *notification* that the window is being destroyed.
        // Since this is our main window, we will quit when this notification
        // is created. The quit message will be found by the message loop in WinMain.
        case WM_DESTROY:
            PostQuitMessage( 0 );
            static_input.quit = 1;
            result = 0;
        break;

        case WM_SIZE:
            result = 0;
        break;

        case WM_LBUTTONDOWN:
        {
            static_input.mousestate = MOUSE_LDOWN;

            result = 0;
        }
        break;

        case WM_LBUTTONUP:
        {
            static_input.mousestate = MOUSE_LUP;
            
            result = 0;
        }
        break;

        case WM_MOUSEMOVE:
        {
            POINT client_mid;
            client_mid.x = window_info.width / 2;
            client_mid.y = window_info.height / 2;
                            
            static_input.mousex += GET_X_LPARAM( lparam ) - client_mid.x;
            static_input.mousey += GET_Y_LPARAM( lparam ) - client_mid.y;
            
            ClientToScreen( window_info.win_handle, &client_mid );
            SetCursorPos( client_mid.x, client_mid.y );
            
            result = 0;
        }
        break;

        case WM_KEYDOWN:
        {
            // If the 30th bit of lparam is set, then the key was already down before this message.
            // We are only interested in the initial keypress, not what happens when it is held.
            if ( !(lparam & (1 << 30)) )
            {
                switch ( wparam )
                {
                case 0x20:
                    static_input.keydown[KeyFireBackwards] = 1;
                    break;

                case 0x1B:
                    static_input.quit = 1;
                    break;

                case 0x80:
                    static_input.pause = !static_input.pause;
                    break;
                }
            }
            result = 0;
        }
        break;

        case WM_KEYUP:
        {
            switch ( wparam )
            {
            case 0x20:
                static_input.keydown[KeyFireBackwards] = 0;
                break;
            }
            result = 0;
        }
        break;

        case WM_SETCURSOR:
        {
            ShowCursor( 0 );
            result = 0;
        }

        default:
            // DefWindowProc is Windows' default action for whatever message is received
            result = DefWindowProc( win_handle, message, wparam, lparam );
        break;
    }

    return result;
}

// Window functions
WindowInfo* InitializeWindowInfo( const char* name, uint32_t width, uint32_t height )
{
    window_info.width = width;
    window_info.height = height;

    window_info.instance = GetModuleHandle( 0 );

    // WNDCLASS is a structure that defines a reusable class for a window.
    window_info.wnd_class = (WNDCLASS) { 0 };

    // These class styles don't really matter, and seem to be archaic. We will use them anyway for historical purposes.
    window_info.wnd_class.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

    // The WindowProcedure we defined above is added to the class.
    window_info.wnd_class.lpfnWndProc = WindowProcedure;

    // The instance of the application this class belongs to is the same instance that we are passed
    window_info.wnd_class.hInstance = window_info.instance;

    // A name for the class, which is not user-visible and is unfortunately necessary.
    window_info.wnd_class.lpszClassName = name;

    // We need to register the window class in order to use it.
    if ( !RegisterClass( &window_info.wnd_class ) )
    {
        init_log( "Failed to register a window class: 0x%lx.\n", GetLastError( ) );
        return NULL;
    }

    RECT window_rect;
    window_rect.left = 0;
    window_rect.top = 0;
    window_rect.right = window_info.width;
    window_rect.bottom = window_info.height;

    AdjustWindowRect( &window_rect, WS_OVERLAPPEDWINDOW, FALSE );

    window_info.win_width = window_rect.right - window_rect.left;
    window_info.win_height = window_rect.bottom - window_rect.top;

    // Having registered the class, we can now create a window with it.
    window_info.win_handle = CreateWindowEx( 0,
                                             name,
                                             name,
                                             WS_VISIBLE | WS_OVERLAPPEDWINDOW,
                                             CW_USEDEFAULT, CW_USEDEFAULT,
                                             window_info.win_width, window_info.win_height,
                                             NULL, NULL, window_info.instance, NULL );

    if ( !window_info.win_handle )
    {
        init_log( "Failed to create a window: 0x%lx.\n", GetLastError( ) );
        return NULL;
    }

    return &window_info;
}

void DestroyWindowInfo( WindowInfo* window )
{
    DestroyWindow( window->win_handle );
}

int CreateWindowSurface( const VkInstance vk_inst, 
                         const WindowInfo* window_info,
                         VkSurfaceKHR* surf )
{
    VkWin32SurfaceCreateInfoKHR surface_create_info = { 0 };
    surface_create_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    surface_create_info.pNext = NULL;
    surface_create_info.hinstance = window_info->instance;
    surface_create_info.hwnd = window_info->win_handle;
    VkResult res = vkCreateWin32SurfaceKHR( vk_inst,
                                            &surface_create_info,
                                            NULL,
                                            surf );
    if ( res )
    {
        init_log( "Failed to create a win32 surface.\n" );
    }
    else
    {
        init_log( "Successfully created win32 rendering surface.\n" );
    }

    return res;
}


uint32_t GetWidth( const WindowInfo* window )
{
    return window->width;
}

uint32_t GetHeight( const WindowInfo* window )
{
    return window->height;
}

void DoWindowRenderUpdate( WindowInfo* window )
{
    ValidateRect( window_info.win_handle, NULL );
}

// Event functions
void ProcessEvents( struct Input* input )
{
    MSG message;

    // Get a message from the window
    while ( PeekMessage( &message, 0, 0, 0, PM_REMOVE ) )
    {
        if ( message.message == WM_QUIT )
        {
            input->quit = 1;
            break;
        }
        else
        {
            TranslateMessage( &message );
            DispatchMessage( &message );
        }
    }

    memcpy( input, &static_input, sizeof( struct Input ) );
    static_input.mousex = 0;
    static_input.mousey = 0;
}

// Timing functions
uint64_t GetTicks();
uint64_t GetTickFrequency();

uint64_t GetTickFrequency( )
{
    LARGE_INTEGER freq;
    // On Windows XP and later, this function is guaranteed to return successfully.
    QueryPerformanceFrequency( &freq );
    return freq.QuadPart;
}

uint64_t GetTicks( )
{
    LARGE_INTEGER count;
    // On Windows XP and later, this function is guaranteed to return successfully.
    QueryPerformanceCounter( &count );
    return count.QuadPart;
}

size_t GetFileSizeFromHandle( HANDLE file )
{
    LARGE_INTEGER size;
    if ( GetFileSizeEx( file, &size ) )
    {
        return (size_t)size.QuadPart;
    }
    else return 0;
}

int ReadBinaryFile( HANDLE file, uint32_t* buf, size_t bufsize, size_t* readsize )
{
#define KB 1024
#define MB (1024 * KB)

    size_t total_read = 0;

    DWORD this_read;
    do
    {
        // Read the file in 1 MB chunks.
        size_t to_read = MB > bufsize - total_read ? bufsize - total_read : MB;

        if ( !ReadFile( file, buf + total_read, to_read, &this_read, NULL ) )
        {
            // Uh-oh an error occurred.
            *readsize = total_read;
            return 1;
        }

        total_read += this_read;
    } while ( this_read != 0 && total_read < bufsize );

    *readsize = total_read;
    return 0;
}

uint32_t* ReadBinaryFileMalloc( const char* filename, size_t* size )
{
    HANDLE hfile = CreateFile( filename,
        GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL );

    if ( hfile == INVALID_HANDLE_VALUE )
    {
        return NULL;
    }

    size_t file_size = GetFileSizeFromHandle( hfile );
    size_t u32_size = file_size / sizeof( uint32_t ) + !!(file_size % sizeof( uint32_t ));
    size_t total_size = u32_size * sizeof( uint32_t );
    uint32_t* result = malloc( total_size );
    if ( !result )
    {
        return NULL;
    }

    ReadBinaryFile( hfile, result, total_size, size );

    CloseHandle( hfile );

    return result;
}

size_t ReadFileSize( const char* filename )
{
    HANDLE hfile = CreateFile( filename,
        GENERIC_READ,
        FILE_SHARE_READ,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL );

    if ( hfile == INVALID_HANDLE_VALUE )
    {
        return 0;
    }

    return GetFileSizeFromHandle( hfile );
}

